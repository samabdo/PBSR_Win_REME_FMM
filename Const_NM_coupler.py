from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
import constrNMPy as cNM

set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding
dz = 2e-6

wcn0 = 0.38e-6
wcw0 = 0.95e-6
wcn2 = 0.43e-6
wcw2 = 0.86e-6
wgp = 0.15e-6
####### Parameter definition
# w[0] = wcn2
# w[1] = wcw2
###########################

def coupling_co(w):  ###a function to calculate the coupling coefficient of a linear taper
    print '===================================================='
    ##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
    wtot = 6e-6
    ycentre = wtot / 2  ###ridge centre y location
    wcn1 = (wcn0 + w[0])/2
    wcw1 = (wcw0 + w[1])/2
    w_clad = (wtot - wcn1 - wgp - wcw1) / 2
    xsin = t_substrate + t_core / 2  ###ridge centre x location of wg

    yr = ycentre + 0.5 * wgp
    yl = ycentre - 0.5 * wgp

    yleft1 = yl - wcn1
    yright1 = yr + wcw1
    yleft0 = yl - wcn0
    yright0 = yr + wcw0
    yleft2 = yl - w[0]
    yright2 = yr + w[1]

    wsl = yleft0 - yleft2  ###width of left stripe
    wsr = yright0 - yright2  ###width of right stripe
    stripe_t = t_core  ###Stripes height
    print 'left stripe width =', wsl
    print 'right stripe width =', wsr
    #############################################################################

    ##### 3 - Define wg_1
    s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
    s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
    rwg_wire = reme.RWG(s_clad(w_clad) + s_core(wcn1) + s_clad(wgp) + s_core(wcw1) + s_clad(w_clad))
    ########################

    ####### 4 - Defining wg_0 and wg_2

    wg_0 = reme.RWG(s_clad(yleft0) + s_core(wcn0) + s_clad(wgp) + s_core(wcw0) + s_clad(wtot - yright0))
    wg_2 = reme.RWG(s_clad(yleft2) + s_core(w[0]) + s_clad(wgp) + s_core(w[1]) + s_clad(wtot - yright2))

    #####################################

    ####### 5 - Getting n0 and n2
    n0l = wg_0.get_refractive_index(xsin, yleft1).real
    print 'n0l =', n0l
    n2l = wg_2.get_refractive_index(xsin, yleft1).real
    print 'n2l =', n2l

    #################################

    ####### 6 - Calculating dn2/dz and constructing an array for dn2/dz
    dnl = n2l - n0l
    print 'dnl =', dnl
    dn2_dzl = (n2l ** 2 - n0l ** 2) / (2 * dz)
    print 'dn2/dzl =', dn2_dzl
    nxx = 100
    nyy = 100
    dn2_dz_arrayl = dn2_dzl * np.ones((nxx, nyy))

    ####!!!!!!!!!!!!!!!!because the gradient in the width is exactly the same on left and right, one can just reverse the sign of dn2/dz
    dn2_dz_arrayr = -1 * dn2_dz_arrayl

    ####################################################################
    #
    ######### 7 - Using FMM solver and finding modes
    guide = reme.FMMStraight(rwg_wire)
    guide.set_left_boundary(reme.PEC)
    guide.set_right_boundary(reme.PEC)
    guide.set_top_boundary(reme.PEC)
    guide.set_bottom_boundary(reme.PEC)

    guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    ################################################

    ######## 8 - Calculating betas and defining constants
    k0 = 2.0 * np.pi / wavelength
    epsilon0 = 8.854187817e-12
    mu0 = 4.0e-7 * np.pi
    beta0 = k0 * guide.get_mode_effective_index(0).real
    beta1 = k0 * guide.get_mode_effective_index(1).real
    beta2 = k0 * guide.get_mode_effective_index(2).real
    dx = stripe_t / (nxx - 1)
    dy = wsl / (nyy - 1)
    ################################################

    ####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
    xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    yleft = np.linspace(yleft2, yleft0, nyy)
    yright = np.linspace(yright2, yright0, nyy)

    e0_leftx = np.zeros((nxx, nyy), complex)
    e1_leftx = np.zeros((nxx, nyy), complex)
    e2_leftx = np.zeros((nxx, nyy), complex)
    e0_lefty = np.zeros((nxx, nyy), complex)
    e1_lefty = np.zeros((nxx, nyy), complex)
    e2_lefty = np.zeros((nxx, nyy), complex)
    e0_leftz = np.zeros((nxx, nyy), complex)
    e1_leftz = np.zeros((nxx, nyy), complex)
    e2_leftz = np.zeros((nxx, nyy), complex)
    e0_rightx = np.zeros((nxx, nyy), complex)
    e1_rightx = np.zeros((nxx, nyy), complex)
    e2_rightx = np.zeros((nxx, nyy), complex)
    e0_righty = np.zeros((nxx, nyy), complex)
    e1_righty = np.zeros((nxx, nyy), complex)
    e2_righty = np.zeros((nxx, nyy), complex)
    e0_rightz = np.zeros((nxx, nyy), complex)
    e1_rightz = np.zeros((nxx, nyy), complex)
    e2_rightz = np.zeros((nxx, nyy), complex)

    for i in range(0, nxx):
        for j in range(0, nyy):
            ####field at left stripe
            e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
            e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
            e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
            e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
            e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
            e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
            e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
            e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
            e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
            ####field at right stripe
            e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
            e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
            e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
            e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
            e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
            e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
            e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
            e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
            e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z
    ##############################################################################################

    ####### 10 - Calculating coupling coefficient
    c01l = 0.0
    c02l = 0.0
    c12l = 0.0
    c01r = 0.0
    c02r = 0.0
    c12r = 0.0
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0

    for i in range(nxx):  ###Dot product of the fields at the left and right stripes
        for j in range(nyy):
            c01l += np.conjugate(e0_leftx[i][j]) * e1_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j]) * \
                    e1_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j]) * e1_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c02l += np.conjugate(e0_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j]) * \
                    e2_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j]) * e2_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c12l += np.conjugate(e1_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e1_lefty[i][j]) * \
                    e2_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e1_leftz[i][j]) * e2_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c01r += np.conjugate(e0_rightx[i][j]) * e1_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e0_righty[i][j]) * e1_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j]) * e1_rightz[i][
                        j] * dn2_dz_arrayr[i][j]
            c02r += np.conjugate(e0_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e0_righty[i][j]) * e2_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j]) * e2_rightz[i][
                        j] * dn2_dz_arrayr[i][j]
            c12r += np.conjugate(e1_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e1_righty[i][j]) * e2_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e1_rightz[i][j]) * e2_rightz[i][
                        j] * dn2_dz_arrayr[i][j]

    c01 = c01l + c01r
    c02 = c02l + c02r
    c12 = c12l + c12r

    c01 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta1) * c01 * (dx * dy)
    c01 = c01 / (beta0 - beta1)
    c02 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta2) * c02 * (dx * dy)
    c02 = c02 / (beta0 - beta2)
    c12 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta1 - beta2) * c12 * (dx * dy)
    c12 = abs(c12) / (beta1 - beta2)

    # print (c01)
    # print (c02)
    if abs(c12)>=0.0999 and abs(c12)<=0.1:
        c12 = 0
    print (c12)

    return c12

# def f(x):
#     z = x[0]**2 + x[1]**2
#     return z


LB=[wcn0,wcw2]
UB=[wcn2,wcw0]
wna = (wcn0+wcn2)/2
wwa = (wcw0+wcw2)/2
wi = [wna,wwa]

res=cNM.constrNM(coupling_co,wi,LB,UB,full_output=True)

# Print results
cNM.printDict(res)




# minimum[0]