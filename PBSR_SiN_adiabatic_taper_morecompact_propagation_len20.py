# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 13:02:03 2018

@author: Islam Abdo
email: islam.abdo@student.rmit.edu.au
"""

####################################################################
#
# Taper from Dai et al 2011, simplified (without short taper sections)
#
####################################################################

#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
import time as time
from scipy.interpolate import interp1d
import pickle

set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding

# Define parameters: taper starting and end widths
w0 = 1.2e-6
w3 = 2.7e-6

w_total = 5e-6



with open('PBSR_SiN_adiabatic_taper_morecompact_w', 'rb') as f:
    w = pickle.load(f)
w = w*1e-6

# Taper length
#length = 200e-6 #length at which w3=2.7
length = 20e-6

###
z = np.linspace(0,length,len(w))

#device in x direction (vertical)
s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
###

xcut = t_substrate + t_core/2



steps = len(w)-1
device1 = Device()

dz = length/steps;



#print (w)
plt.plot(z*1e6, w*1e6, 'b')
plt.xlabel('z (um)')
plt.ylabel('w (um)')
plt.show()
#
#Create taper
wgs_wire=list()
wgs = list()
y2 = np.zeros(steps)
y1 = np.zeros(steps)

for i in range (0, steps, 1):
    
    # Need to make the waveguide symmetric.
    # You can reduce the widths of the cladding on both sides of the waveguide core.
    
    
    y2[i] = w[i]/2
    y1[i] = - w[i]/2
    ycore = y2[i]-y1[i]
    yclad = (w_total - ycore)/2

    wg = FMMStraight(3)

    wg.set_slab(1, s_clad, yclad)
    wg.set_slab(2, s_core, ycore)
    wg.set_slab(3, s_clad, yclad)

    wg.set_left_boundary(PEC)
    wg.set_right_boundary(PEC)
    wg.set_degenerate(True)

    wgs.append(wg)
    device1.add_waveguide(wg, dz)
    
#device1.show()
    


# View the refractive index distribution
device1.view_refractive_index_Xcut(xcut, 1.5e-6, 3.5e-6, 1000, 0, length, 1000)
#
#
#
print "Here at TE excitation"
# Excite the device with TM0 mode, amplitude 1.0
device1.set_incident_left(1, 1.0)
print "Here at calculate"
device1.calculate()
print "Done with calculate"
#print "Refelected power ", abs(device1.get_left_output_port(0, 0))**2
#print "Output power: ", abs(device1.get_right_output_port(0, 0))**2
#
# View the field
device1.view_field_Xcut('E', xcut, 1.5e-6, 3.5e-6, 1000, 0, length, 1000)
#
#device1.view_field_Zcut('E', 0, 0, 2.22e-6, 1000, 1.5e-6, 2.5e-6, 1000)
#device1.view_field_Zcut('E', length, 0, 2.22e-6, 1000, 1.5e-6, 2.5e-6, 1000)


# View the waveguide mode evolution
device1.view_mode_evolution()

# Get amplitude of output mode
print("Output TE1 mode amplitude: {}".format(abs(device1.get_output_right(1))))
print("Output TM0 mode amplitude: {}".format(abs(device1.get_output_right(2))))

# You can view the mode field of diffent waveguide sections

#wgs[0].view_modes()
#wgs[steps/2].view_modes()
#wgs[steps-1].view_modes()

## Re-simulate with different taper length
#
## NOTE: The taper devices consists of elements. The first element is a WaveguideSection, the second element is a Joint, the third element is a WaveguideSection, ...
#
## To change the taper length, just change the length of each waveguide section. 
#x1 = 0 #TE1 amplitude
#x2 = 0 #TM0 amplitude
#num_points = 100
#x = np.zeros(num_points) #TE1
#y = np.zeros(num_points) #TM0
#
#
#ltp2 = np.linspace(5e-6, 100e-6, num_points)
#
#for k in range (0, num_points, 1):
#    dz = ltp2[k]/steps;
#    for i in range (0, steps, 1):
#        element = device1.get_element(i*2)
#        element.set_length(dz)
##    taper = Device([taper1, taper2, taper3])
#    device1.set_incident_left(1, 1.0)
#    device1.calculate() 
#    x1 = abs(device1.get_output_right(1))
#    x2 = abs(device1.get_output_right(2))
#    x[k] = x1**2/(x1**2+x2**2)
#    y[k] = x2**2/(x1**2+x2**2)
#
#
#
#plt.plot(ltp2*1e6, x*100, 'r', ltp2*1e6, y*100, 'b')
#plt.xlabel('Taper Length (um)')
#plt.ylabel('Energy Conversion Efficiency (%)')
#plt.show()