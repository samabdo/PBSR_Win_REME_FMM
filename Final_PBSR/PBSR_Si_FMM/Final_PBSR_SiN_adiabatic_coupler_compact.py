#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Construct a nonlinear taper that supports TE1 and TM0 modes such that coupling between the two modes is maintained below a certain value

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays
from scipy.interpolate import interp1d
import pickle


####Global variables
limit = 3e-9  ###minimum limit on strip width

        



def golden_search_coupler(c1, c2, w0n, w0w, w2n_arr, w2w_arr, dz, n):  ### Golden search function
    print '~~~~~~~~~~~~~~~~~~~~~SEARCHING~~~~~~~~~~~~~~~~~~~~'
    print '       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        '
    print '              ~~~~~~~~~~~~~~~~~~~~~               '
    print '                   ~~~~~~~~~~~                    '

    #!!!!!!!!!!!!!!!!!Assuming that c1 and c2 lies between a, b and c
    no_interv = 5
    interv_len = len(w2n_arr)/no_interv
    if n == 0:
        a = 0
    else:
        a = np.argwhere(w2n_arr == w0n)
        a = a[0,0]
    c = a + interv_len
    if c > len(w2n_arr)-1:
        c = len(w2n_arr)-1
    b = (a+c)/2
    
    w2na = w2n_arr[a]
    w2nb = w2n_arr[b]
    w2nc = w2n_arr[c]
    w2wa = w2w_arr[a]
    w2wb = w2w_arr[b]
    w2wc = w2w_arr[c]    
    
    w1na = (w2na+w0n)/2
    w1nb = (w2nb+w0n)/2
    w1nc = (w2nc+w0n)/2
    w1wa = (w2wa+w0w)/2
    w1wb = (w2wb+w0w)/2
    w1wc = (w2wc+w0w)/2
    
    c01a, c02a, c12a = coupling_co(w0n, w0w,w1na, w1wa,w2na, w2wa,dz)
    c01b, c02b, c12b = coupling_co(w0n, w0w,w1nb, w1wb,w2nb, w2wb,dz)
    c01c, c02c, c12c = coupling_co(w0n, w0w,w1nc, w1wc,w2nc, w2wc,dz)
    
    
    if abs(c12c)<abs(c12b) and abs(c12b)<abs(c12a):   ######left part of the graph
        i = 0
        left = c # Determines the starting index of the list we have to search in
        right = a
        mid = b    
        w2n = w2n_arr[mid]
        w2w = w2w_arr[mid]
        print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
        print '$$$$$$$$$$$$$$$$$$$$$$$$$'
        w1n = (w2n+w0n)/2
        w1w = (w2w+w0w)/2
        c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz) #!!!!!!!!!!!!!!!!!!!redundant. It can be replaced by c12b
        while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
            # If the current middle element is less than x then move the left next to mid
            # Else we move right next to mid
            if i > 20:
                break
            if  abs(c12) < c1:
                left = mid + 1
            if  abs(c12) > c2:
                right = mid - 1
            mid = (right + left)/2
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
            print 'left', w2_arr[left]
            print 'w2',w2
            print 'right', w2_arr[right]
            print '$$$$$$$$$$$$$$$$$$$$$$$$$'
            w1n = (w2n+w0n)/2
            w1w = (w2w+w0w)/2
            c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz)
            i+=1
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        
    elif abs(c12a)<abs(c12b) and abs(c12b)<abs(c12c):   #####right part of the graph
        i = 0
        left = c # Determines the starting index of the list we have to search in
        right = a
        mid = b  
        w2n = w2n_arr[mid]
        w2w = w2w_arr[mid]
        print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
        print '&&&&&&&&&&&&&&&&&&&&&&&&&'
        w1n = (w2n+w0n)/2
        w1w = (w2w+w0w)/2
        c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz)
        while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
            # If the current middle element is less than x then move the left next to mid
            # Else we move right next to mid
            if i > 20:
                break
            if  abs(c12) < c1:
                right = mid - 1
            if  abs(c12) > c2:
                left = mid + 1
            mid = (right + left)/2
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
            print '&&&&&&&&&&&&&&&&&&&&&&&&&'
            w1n = (w2n+w0n)/2
            w1w = (w2w+w0w)/2
            c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz)
            i+=1
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        
    elif abs(c12a)<abs(c12b) and abs(c12c)<abs(c12b):   #####peak part of the graph
        if (c1 > abs(c12a) and c1 < abs(c12b)) or (c2 > abs(c12a) and c2 < abs(c12b)):
            i = 0
            left = b # Determines the starting index of the list we have to search in
            right = a
            mid = (left+right)/2  
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            print '&&&&&&&&&Welcome to the peak!!!!!&&&&&&&&&&&'
            print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
            print '&&&&&&&&&&&&&&&&&&&&&&&&&'
            w1n = (w2n+w0n)/2
            w1w = (w2w+w0w)/2
            c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz)
            while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
                # If the current middle element is less than x then move the left next to mid
                # Else we move right next to mid
                if i > 20:
                    break
                if  abs(c12) < c1:
                    right = mid - 1
                if  abs(c12) > c2:
                    left = mid + 1
                mid = (right + left)/2
                w2n = w2n_arr[mid]
                w2w = w2w_arr[mid]
                print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
                print '&&&&&&&&&&&&&&&&&&&&&&&&&'
                w1n = (w2n+w0n)/2
                w1w = (w2w+w0w)/2
                c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz)
                i+=1
            print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
            print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        elif (c1 > abs(c12c) and c1 < abs(c12b)) or (c2 > abs(c12c) and c2 < abs(c12b)):
            i = 0
            left = c # Determines the starting index of the list we have to search in
            right = b
            mid = (left+right)/2    
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            print '$$$$$$$$$$Welcome to the peak!!!!!$$$$$$$$$$$'
            print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
            print '$$$$$$$$$$$$$$$$$$$$$$$$$'
            w1n = (w2n+w0n)/2
            w1w = (w2w+w0w)/2
            c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz) #!!!!!!!!!!!!!!!!!!!redundant. It can be replaced by c12b
            while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
                # If the current middle element is less than x then move the left next to mid
                # Else we move right next to mid
                if i > 20:
                    break
                if  abs(c12) < c1:
                    left = mid + 1
                if  abs(c12) > c2:
                    right = mid - 1
                mid = (right + left)/2
                w2n = w2n_arr[mid]
                w2w = w2w_arr[mid]
                print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
                print '$$$$$$$$$$$$$$$$$$$$$$$$$'
                w1n = (w2n+w0n)/2
                w1w = (w2w+w0w)/2
                c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,dz)
                i+=1
            print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
            print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w                
        
    return w1n, w1w, w2n, w2w, c01, c02, c12

def stripe_w(w0,w2): ### Afunction to get the stripe width of a linear taper
    wtot = 6e-6
    ycentre = wtot/2  ###ridge centre y location
    
    yleft0 = ycentre - w0/2
    yright0 = ycentre + w0/2
    yleft2 = ycentre - w2/2
    yright2 = ycentre + w2/2
    
    wsl = yleft2 - yleft0 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    return wsl, wsr
    
def max_w2 (w0,limit):  #### getting the maximum finish width of a slice so that the stripe width is greater than a limit
    w2_arr = np.linspace(w0,0,5000)
    for i in range(0, len(w2_arr)):
        wsl, wsr = stripe_w(w0,w2_arr[i])
        if (wsl >= limit and wsr >= limit):
            w2 = w2_arr[i]
            break
    return w2
    
def coupling_co(wcn0, wcw0,wcn1, wcw1, wcn2, wcw2,dz): ###a function to calculate the coupling coefficient of a linear taper
    print '===================================================='
    ##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
    wtot = 6e-6
    ycentre = wtot/2  ###ridge centre y location
    w_clad = (wtot - wcn1 - wgp - wcw1)/2
    xsin = t_substrate + t_core/2  ###ridge centre x location of wg
    
    yr = ycentre + 0.5*wgp
    yl = ycentre - 0.5*wgp
    
    yleft1 = yl - wcn1
    yright1 = yr + wcw1
    yleft0 = yl - wcn0
    yright0 = yr + wcw0
    yleft2 = yl - wcn2
    yright2 = yr + wcw2
    
    wsl = yleft0 - yleft2 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    stripe_t = t_core ###Stripes height
    print 'left stripe width =', wsl
    print 'right stripe width =', wsr
    #############################################################################
    
    
    ##### 3 - Define wg_1
    s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
    s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
    rwg_wire = reme.RWG(s_clad(w_clad) + s_core(wcn1) + s_clad(wgp) + s_core(wcw1) + s_clad(w_clad))
    ########################
    
    ####### 4 - Defining wg_0 and wg_2

    
    wg_0 = reme.RWG(s_clad(yleft0) + s_core(wcn0) + s_clad(wgp) + s_core(wcw0) + s_clad(wtot - yright0))
    wg_2 = reme.RWG(s_clad(yleft2) + s_core(wcn2) + s_clad(wgp) + s_core(wcw2) + s_clad(wtot - yright2))

    #####################################
    
    ####### 5 - Getting n0 and n2
    n0l = wg_0.get_refractive_index(xsin, yleft1).real
    print 'n0l =',n0l    
    n2l = wg_2.get_refractive_index(xsin, yleft1).real
    print 'n2l =',n2l
    
    #################################
    
    ####### 6 - Calculating dn2/dz and constructing an array for dn2/dz
    dnl = n2l - n0l
    print 'dnl =', dnl  
    dn2_dzl = (n2l**2 - n0l**2)/(2*dz)
    print 'dn2/dzl =', dn2_dzl
    nxx = 100
    nyy = 100
    dn2_dz_arrayl = dn2_dzl*np.ones((nxx,nyy))
    
    
    ####!!!!!!!!!!!!!!!!because the gradient in the width is exactly the same on left and right, one can just reverse the sign of dn2/dz
    dn2_dz_arrayr = -1*dn2_dz_arrayl 
    

    ####################################################################
    #
    ######### 7 - Using FMM solver and finding modes
    guide = reme.FMMStraight(rwg_wire)
    guide.set_left_boundary(reme.PEC)
    guide.set_right_boundary(reme.PEC)
    guide.set_top_boundary(reme.PEC)
    guide.set_bottom_boundary(reme.PEC)

    guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    ################################################
    
    ######## 8 - Calculating betas and defining constants
    k0 = 2.0*np.pi/wavelength
    epsilon0 =  8.854187817e-12
    mu0 = 4.0e-7*np.pi
    beta0 = k0*guide.get_mode_effective_index(0).real
    beta1 = k0*guide.get_mode_effective_index(1).real
    beta2 = k0*guide.get_mode_effective_index(2).real
    dx = stripe_t/(nxx-1)
    dy = wsl/(nyy-1)
    ################################################
    
    ####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
    xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    yleft = np.linspace(yleft2, yleft0, nyy)
    yright = np.linspace(yright2, yright0, nyy)
    
    e0_leftx = np.zeros((nxx, nyy), complex)
    e1_leftx = np.zeros((nxx, nyy), complex)
    e2_leftx = np.zeros((nxx, nyy), complex)
    e0_lefty = np.zeros((nxx, nyy), complex)
    e1_lefty = np.zeros((nxx, nyy), complex)
    e2_lefty = np.zeros((nxx, nyy), complex)
    e0_leftz = np.zeros((nxx, nyy), complex)
    e1_leftz = np.zeros((nxx, nyy), complex)
    e2_leftz = np.zeros((nxx, nyy), complex)
    e0_rightx = np.zeros((nxx, nyy), complex)
    e1_rightx = np.zeros((nxx, nyy), complex)
    e2_rightx = np.zeros((nxx, nyy), complex)
    e0_righty = np.zeros((nxx, nyy), complex)
    e1_righty = np.zeros((nxx, nyy), complex)
    e2_righty = np.zeros((nxx, nyy), complex)
    e0_rightz = np.zeros((nxx, nyy), complex)
    e1_rightz = np.zeros((nxx, nyy), complex)
    e2_rightz = np.zeros((nxx, nyy), complex)
    
    for i in range(0, nxx):
            for j in range (0, nyy):
                ####field at left stripe
                e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
                e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
                e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
                e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
                e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
                e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
                e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
                e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
                e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
                ####field at right stripe
                e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
                e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
                e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
                e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
                e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
                e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
                e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
                e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
                e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z
    ##############################################################################################            
                
    ####### 10 - Calculating coupling coefficient
    c01l = 0.0
    c02l = 0.0
    c12l = 0.0
    c01r = 0.0
    c02r = 0.0
    c12r = 0.0
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0
    
    for i in range(nxx): ###Dot product of the fields at the left and right stripes
            for j in range(nyy):
                c01l += np.conjugate(e0_leftx[i][j])*e1_leftx[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j])*e1_lefty[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j])*e1_leftz[i][j]*dn2_dz_arrayl[i][j]
                c02l += np.conjugate(e0_leftx[i][j])*e2_leftx[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j])*e2_lefty[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j])*e2_leftz[i][j]*dn2_dz_arrayl[i][j]
                c12l += np.conjugate(e1_leftx[i][j])*e2_leftx[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e1_lefty[i][j])*e2_lefty[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e1_leftz[i][j])*e2_leftz[i][j]*dn2_dz_arrayl[i][j]
                c01r += np.conjugate(e0_rightx[i][j])*e1_rightx[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_righty[i][j])*e1_righty[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j])*e1_rightz[i][j]*dn2_dz_arrayr[i][j]
                c02r += np.conjugate(e0_rightx[i][j])*e2_rightx[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_righty[i][j])*e2_righty[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j])*e2_rightz[i][j]*dn2_dz_arrayr[i][j]
                c12r += np.conjugate(e1_rightx[i][j])*e2_rightx[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e1_righty[i][j])*e2_righty[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e1_rightz[i][j])*e2_rightz[i][j]*dn2_dz_arrayr[i][j]
    
    c01 = c01l + c01r
    c02 = c02l + c02r
    c12 = c12l + c12r
    
    c01 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta1)*c01*(dx*dy)
    c01 = c01/(beta0-beta1)
    c02 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta2)*c02*(dx*dy)
    c02 = c02/(beta0-beta2)
    c12 = (epsilon0/mu0)**0.5*k0/4.0/(beta1-beta2)*c12*(dx*dy)
    c12 = c12/(beta1-beta2)
    
    print (c01)
    print (c02)
    print (c12)
    
    return c01,c02,c12


#========================================================= Nonlinear taper construction =============================================================================
set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding

c01_array = []
c02_array = []
c12_array = []
wn_array = []
ww_array = []
z_array = []


##==========================================Algorithm for nonlinear taper optimization====================================#
##### 1 - Define dimensions of first segment

w00 = 0.38e-6
w0 = 0.43e-6
w3 = 0.95e-6
w4 = 0.86e-6
wgp = 0.15e-6
wtot = 6e-6
dz = 0.5e-6
limit = 3e-9  ###minimum limit on strip width
w2w_arr = np.linspace(w3,w4-0.5e-6,100000)  ###finish width of each slice
w2n_arr = np.linspace(w00,w0+0.5e-6,100000)
w0n = w00
w0w = w3
########################################################


#### 5 - Doing Bisection search along the taper until the finish width of the taper is equal to we
n = 0  ### number of 2dz's
c1 = 0.0999 #### lower bound for coupling coefficient
c2 = 0.1 #### higher bound for coupling coefficient

while (w0n <= w0): 
    z_array.append(n*2*dz)
    ##### 2 - Calculate coupling coefficients
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0
    w1n = 0.0
    w1n = 0.0
    w2w = 0.0
    w2w = 0.0
    
    w1n, w1w, w2n, w2w, c01, c02, c12 = golden_search_coupler(c1, c2, w0n, w0w, w2n_arr, w2w_arr, dz, n)   ###Searching for w1 that satisfy c12 in the range c1 to c2
#    print w2
    wn_array.append(w1n)
    ww_array.append(w1w)
    c01_array.append(c01)
    c02_array.append(c02)
    c12_array.append(c12)
    
    w0n = w2n  ####setting start width of next slice equals to the finish width of current slice
    w0w = w2w    
    n += 1  ###getting the number of 2dz's in order to get the total length of the taper
#####################################################################################################
##========================================================================================================================#

######Converting the arrays to fixed sized arrays to allow multiplication with float
z = np.zeros(len(z_array))
wn = np.zeros(len(wn_array))
ww = np.zeros(len(wn_array))
c01 = np.zeros(len(c01_array))
c02 = np.zeros(len(c02_array))
c12 = np.zeros(len(c12_array))

for i in range (0, len(z)):
    z[i] = z_array[i]*1e6
    wn[i] = wn_array[i]*1e6
    ww[i] = ww_array[i]*1e6
    c01[i] = c01_array[i]
    c02[i] = c02_array[i]
    c12[i] = c12_array[i]
###################################################################################


#writing to files

with open('Final_PBSR_adiabatic_coupler_compact_z', 'wb') as f:
    pickle.dump(z, f)
    
with open('Final_PBSR_adiabatic_coupler_compact_wn', 'wb') as f:
    pickle.dump(wn, f)
    
with open('Final_PBSR_adiabatic_coupler_compact_ww', 'wb') as f:
    pickle.dump(ww, f)
    
with open('Final_PBSR_adiabatic_coupler_compact_c01', 'wb') as f:
    pickle.dump(c01, f)
    
with open('Final_PBSR_adiabatic_coupler_compact_c02', 'wb') as f:
    pickle.dump(c02, f)
    
with open('Final_PBSR_adiabatic_coupler_compact_c12', 'wb') as f:
    pickle.dump(c12, f)

######Plotting w, c01, c02 and c12
plt.plot(z, wn, 'r')
plt.xlabel('z (um)')
plt.ylabel('narrow waveguide (um)')
plt.show()

plt.plot(z, ww, 'b')
plt.xlabel('z (um)')
plt.ylabel('wide waveguide(um)')
plt.show()

#plt.plot(z, c01, 'r', z, c02, 'b', z, c12, 'g')
#plt.xlabel('z (um)')
#plt.ylabel('coupling coefficient')
#plt.show()
    
plt.plot(z, abs(c01), 'r', z, abs(c02), 'b', z, abs(c12), 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.ylim(0, 1.1)
plt.show()
##################################################################################
    
##reading from files
#    
#with open('LNOI_PBSR_adiabatic_taper_z', 'rb') as f:
#    z_file = pickle.load(f)
#
#with open('LNOI_PBSR_adiabatic_taper_w', 'rb') as f:
#    w_file = pickle.load(f)
#
#with open('LNOI_PBSR_adiabatic_taper_c01', 'rb') as f:
#    c01_file = pickle.load(f)
#    
#with open('LNOI_PBSR_adiabatic_taper_c02', 'rb') as f:
#    c02_file = pickle.load(f)
#    
#with open('LNOI_PBSR_adiabatic_taper_c12', 'rb') as f:
#    c12_file = pickle.load(f)