# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 13:02:03 2018

@author: Islam Abdo
email: islam.abdo@student.rmit.edu.au
"""

####################################################################
#
# Taper from Dai et al 2011, simplified (without short taper sections)
#
####################################################################

#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
import time as time
from scipy.interpolate import interp1d
import pickle

########### Materials and Substrates ##############
set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding
###################################################

# Define parameters: taper starting and end widths
wgp1 = 0.15e-6

w_total = 10e-6
w_center = w_total/2

#++++++++++++++++++++loading files++++++++++++
############Adiabatic taper###########
with open('PBSR_SiN_adiabatic_taper_morecompact_z', 'rb') as f:
    zt = pickle.load(f)
zt = zt*1e-6
with open('PBSR_SiN_adiabatic_taper_morecompact_w', 'rb') as f:
    wt = pickle.load(f)
wt = wt*1e-6

############Adiabatic coupler##########

with open('LNOI_PBSR_adiabatic_coupler_compact_z', 'rb') as f:
    zc = pickle.load(f)
zc = zc*1e-6
with open('LNOI_PBSR_adiabatic_coupler_compact_wn', 'rb') as f:
    wnc = pickle.load(f)
wnc = wnc*1e-6
with open('LNOI_PBSR_adiabatic_coupler_compact_ww', 'rb') as f:
    wwc = pickle.load(f)
wwc = wwc*1e-6

############Output region#############
n = 25
with open('LNOI_PBSR_adiabatic_outputregion_norad_z', 'rb') as f:
    zol = pickle.load(f)
zo = np.zeros(n)
for i in range (0, n, 1):
    zo[i] = zol[i]
zo = zo*1e-6
with open('LNOI_PBSR_adiabatic_outputregion_norad_wn', 'rb') as f:
    wnol = pickle.load(f)
wno = np.zeros(n)
for i in range (0, n, 1):
    wno[i] = wnol[i]
wno = wno*1e-6
with open('LNOI_PBSR_adiabatic_outputregion_norad_ww', 'rb') as f:
    wwol = pickle.load(f)
wwo = np.zeros(n)
for i in range (0, n, 1):
    wwo[i] = wwol[i]
wwo = wwo*1e-6
with open('LNOI_PBSR_adiabatic_outputregion_norad_wgp', 'rb') as f:
    wgpol = pickle.load(f)
wgpo = np.zeros(n)
for i in range (0, n, 1):
    wgpo[i] = wgpol[i]
wgpo = wgpo*1e-6
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++

length_t = zt[len(zt)-1]# Taper length
length_c = zc[len(zc)-1]# Coupler length
length_o = zo[len(zo)-1]# Output region length
ltot = length_t + length_c + length_o

#device in x direction (vertical)
s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
###

xcut = t_substrate + t_core/2



stepst = len(zt)-1
stepsc = len(zc)-1
stepso = len(zo)-1


device1 = Device()  ###Taper
device2 = Device()  ###Coupler
device3 = Device()  ###Output region
PBSR = Device()     ###PBSR

dzt = length_t/stepst;
dzc = length_c/stepsc;
dzo = length_o/stepso;

#_____________plotting widths________________
#__________ Plotting widths___________
# plt.plot(zt*1e6, wt*1e6, 'b')
# plt.xlabel('z_taper (um)')
# plt.ylabel('w_taper (um)')
# plt.show()
#
# plt.plot(zc*1e6, wnc*1e6, 'b')
# plt.xlabel('z_coupler (um)')
# plt.ylabel('wn_coupler (um)')
# plt.show()
#
# plt.plot(zc*1e6, wwc*1e6, 'b')
# plt.xlabel('z_coupler (um)')
# plt.ylabel('ww_coupler (um)')
# plt.show()
#
# plt.plot(zo*1e6, wno*1e6, 'b')
# plt.xlabel('z_output region (um)')
# plt.ylabel('wn_output region (um)')
# plt.show()
#
# plt.plot(zo*1e6, wwo*1e6, 'b')
# plt.xlabel('z_output region (um)')
# plt.ylabel('ww_output region (um)')
# plt.show()
#
# plt.plot(zo*1e6, wgpo*1e6, 'b')
# plt.xlabel('z_output region (um)')
# plt.ylabel('wgp_output region (um)')
# plt.show()

#Create taper
#_____________________________________________


wgs_wire=list()
wgs = list()
w_center2 = w_center - wwo[0]/2 - 0.5*wgp1

#===========Creating devices==============
for i in range (0, stepst, 1):
    ###########taper########
    y2t = w_center + wt[i]/2
    y1t = w_center - wt[i]/2
    ycoret = y2t-y1t

    wgt = FMMStraight(3)

    wgt.set_slab(1, s_clad, y1t)
    wgt.set_slab(2, s_core, ycoret)
    wgt.set_slab(3, s_clad, w_total - y2t)

    wgt.set_left_boundary(PEC)
    wgt.set_right_boundary(PEC)
    wgt.set_degenerate(True)

    wgs.append(wgt)
    device1.add_waveguide(wgt, dzt)
    #############################
for i in range(0, stepsc, 1):
    ###########coupler########
    y1c = w_center - wwc[0]/2
    y2c = y1c + wwc[i]
    y4c = y1c - wgp1
    y3c = y4c - wnc[i]

    wgc = FMMStraight(5)

    wgc.set_slab(1, s_clad, y3c)
    wgc.set_slab(2, s_core, y4c - y3c)
    wgc.set_slab(3, s_clad, y1c - y4c)
    wgc.set_slab(4, s_core, y2c - y1c)
    wgc.set_slab(5, s_clad, w_total - y2c)

    wgc.set_left_boundary(PEC)
    wgc.set_right_boundary(PEC)
    wgc.set_degenerate(True)

    wgs.append(wgc)
    device2.add_waveguide(wgc, dzc)
    #############################
for i in range(0, stepso, 1):
    ###########Output Region########
    y1o = w_center2 + 0.5*wgpo[i]
    y2o = y1o + wwo[i]
    y4o = w_center2 - wgpo[i]/2
    y3o = y4o - wno[i]

    wgo = FMMStraight(5)

    wgo.set_slab(1, s_clad, y3o)
    wgo.set_slab(2, s_core, y4o - y3o)
    wgo.set_slab(3, s_clad, y1o - y4o)
    wgo.set_slab(4, s_core, y2o - y1o)
    wgo.set_slab(5, s_clad, w_total - y2o)

    wgo.set_left_boundary(PEC)
    wgo.set_right_boundary(PEC)
    wgo.set_degenerate(True)

    wgs.append(wgo)
    device3.add_waveguide(wgo, dzo)
    #############################
#=========================================
    
PBSR = Device([device1, device2, device3])

# View the refractive index distribution
PBSR.view_refractive_index_Xcut(xcut, 2e-6, 7e-6, 1000, 0, ltot, 1000)

print "Here at TE excitation"
# Excite the device with TM0 mode, amplitude 1.0
PBSR.set_incident_left(0, 1.0)
print "Here at calculate"
PBSR.calculate()
print "Done with calculate"
#
# View the field
PBSR.view_field_Xcut('E', xcut, 2e-6, 7e-6, 1000, 0, ltot, 1000)
#
#device1.view_field_Zcut('E', 0, 0, 2.22e-6, 1000, 1.5e-6, 2.5e-6, 1000)
#device1.view_field_Zcut('E', length, 0, 2.22e-6, 1000, 1.5e-6, 2.5e-6, 1000)


# View the waveguide mode evolution
PBSR.view_mode_evolution()

# Get amplitude of output mode
# print("Output TE1 mode amplitude: {}".format(abs(device1.get_output_right(1))))
# print("Output TM0 mode amplitude: {}".format(abs(device1.get_output_right(2))))

# You can view the mode field of diffent waveguide sections

#wgs[0].view_modes()
#wgs[steps/2].view_modes()
#wgs[steps-1].view_modes()
