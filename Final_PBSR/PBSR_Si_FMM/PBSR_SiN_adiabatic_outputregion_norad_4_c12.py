#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Construct a nonlinear taper that supports TE1 and TM0 modes such that coupling between the two modes is maintained below a certain value

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays
from scipy.interpolate import interp1d
import pickle


####Global variables
limit = 3e-9  ###minimum limit on strip width

        



def golden_search_output(c1, c2, w0n, w0w, wgp0, w2n_arr, w2w_arr,wgp2_arr, dz, n):  ### Golden search function
    print '~~~~~~~~~~~~~~~~~~~~~SEARCHING~~~~~~~~~~~~~~~~~~~~'
    print '       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        '
    print '              ~~~~~~~~~~~~~~~~~~~~~               '
    print '                   ~~~~~~~~~~~                    '

    #!!!!!!!!!!!!!!!!!Assuming that c1 and c2 lies between a, b and c
    no_interv = 5
    interv_len = len(w2n_arr)/no_interv
    if n == 0:
        a = 0
    else:
        a = np.argwhere(w2n_arr == w0n)
        a = a[0,0]
    c = a + interv_len
    if c > len(w2n_arr)-1:
        c = len(w2n_arr)-1
    b = (a+c)/2
    
    w2na = w2n_arr[a]
    w2nb = w2n_arr[b]
    w2nc = w2n_arr[c]
    w2wa = w2w_arr[a]
    w2wb = w2w_arr[b]
    w2wc = w2w_arr[c]
    wgp2a = wgp2_arr[a]
    wgp2b = wgp2_arr[b]
    wgp2c = wgp2_arr[c]

    w1na = (w2na+w0n)/2
    w1nb = (w2nb+w0n)/2
    w1nc = (w2nc+w0n)/2
    w1wa = (w2wa+w0w)/2
    w1wb = (w2wb+w0w)/2
    w1wc = (w2wc+w0w)/2
    wgp1a = (wgp2a + wgp0) / 2
    wgp1b = (wgp2b + wgp0) / 2
    wgp1c = (wgp2c + wgp0) / 2
    
    c01a, c02a, c12a = coupling_co(w0n, w0w,w1na, w1wa,w2na, w2wa, wgp0, wgp1a, wgp2a,dz)
    c01b, c02b, c12b = coupling_co(w0n, w0w,w1nb, w1wb,w2nb, w2wb, wgp0, wgp1b, wgp2c,dz)
    c01c, c02c, c12c = coupling_co(w0n, w0w,w1nc, w1wc,w2nc, w2wc, wgp0, wgp1c, wgp2c,dz)
    
    
    if abs(c12c)<abs(c12b) and abs(c12b)<abs(c12a):   ######left part of the graph
        i = 0
        left = c # Determines the starting index of the list we have to search in
        right = a
        mid = b    
        w2n = w2n_arr[mid]
        w2w = w2w_arr[mid]
        wgp2 = wgp2_arr[mid]
        print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
        print '$$$$$$$$$$$$$$$$$$$$$$$$$'
        w1n = (w2n+w0n)/2
        w1w = (w2w+w0w)/2
        wgp1 = (wgp2+wgp0)/2
        c01, c02, c12 = coupling_co(w0n, w0w,w1n, w1w,w2n, w2w,wgp0, wgp1, wgp2,dz) #!!!!!!!!!!!!!!!!!!!redundant. It can be replaced by c12b
        while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
            # If the current middle element is less than x then move the left next to mid
            # Else we move right next to mid
            if i > 20:
                break
            if  abs(c01) < c1:
                left = mid + 1
            if  abs(c01) > c2:
                right = mid - 1
            mid = (right + left)/2
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            wgp2 = wgp2_arr[mid]
            print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
            print '$$$$$$$$$$$$$$$$$$$$$$$$$'
            w1n = (w2n + w0n) / 2
            w1w = (w2w + w0w) / 2
            wgp1 = (wgp2 + wgp0) / 2
            c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz)
            i+=1
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ', wgp1
        
    elif abs(c12a)<abs(c12b) and abs(c12b)<abs(c12c):   #####right part of the graph
        i = 0
        left = c # Determines the starting index of the list we have to search in
        right = a
        mid = b
        w2n = w2n_arr[mid]
        w2w = w2w_arr[mid]
        wgp2 = wgp2_arr[mid]
        print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
        print '&&&&&&&&&&&&&&&&&&&&&&&&&'
        w1n = (w2n + w0n) / 2
        w1w = (w2w + w0w) / 2
        wgp1 = (wgp2 + wgp0) / 2
        c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz)
        while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
            # If the current middle element is less than x then move the left next to mid
            # Else we move right next to mid
            if i > 20:
                break
            if  abs(c01) < c1:
                right = mid - 1
            if  abs(c01) > c2:
                left = mid + 1
            mid = (right + left)/2
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            wgp2 = wgp2_arr[mid]
            print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
            print '&&&&&&&&&&&&&&&&&&&&&&&&&'
            w1n = (w2n + w0n) / 2
            w1w = (w2w + w0w) / 2
            wgp1 = (wgp2 + wgp0) / 2
            c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz)
            i+=1
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ', wgp1
        
    elif abs(c12a)<abs(c12b) and abs(c12c)<abs(c12b):   #####peak part of the graph
        # if (c1 >= abs(c01a) and c1 < abs(c01b)) or (c2 > abs(c01a) and c2 < abs(c01b)):
        i = 0
        left = b # Determines the starting index of the list we have to search in
        right = a
        mid = (left+right)/2
        w2n = w2n_arr[mid]
        w2w = w2w_arr[mid]
        wgp2 = wgp2_arr[mid]
        print '&&&&&&&&&Welcome to the peak!!!!!&&&&&&&&&&&'
        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
        print '&&&&&&&&&&&&&&&&&&&&&&&&&'
        w1n = (w2n + w0n) / 2
        w1w = (w2w + w0w) / 2
        wgp1 = (wgp2 + wgp0) / 2
        c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz)
        while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
            # If the current middle element is less than x then move the left next to mid
            # Else we move right next to mid
            if i > 20:
                break
            if  abs(c01) < c1:
                right = mid - 1
            if  abs(c01) > c2:
                left = mid + 1
            mid = (right + left)/2
            w2n = w2n_arr[mid]
            w2w = w2w_arr[mid]
            wgp2 = wgp2_arr[mid]
            print '&&&&&&&&&La Droite side of the one slice graph&&&&&&&&&&&'
            print '&&&&&&&&&&&&&&&&&&&&&&&&&'
            w1n = (w2n + w0n) / 2
            w1w = (w2w + w0w) / 2
            wgp1 = (wgp2 + wgp0) / 2
            c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz)
            i+=1
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ', wgp1
        # elif (c1 >= abs(c01c) and c1 < abs(c01b)) or (c2 > abs(c01c) and c2 < abs(c01b)):
        #     i = 0
        #     left = c # Determines the starting index of the list we have to search in
        #     right = b
        #     mid = (left+right)/2
        #     w2n = w2n_arr[mid]
        #     w2w = w2w_arr[mid]
        #     wgp2 = wgp2_arr[mid]
        #     print '$$$$$$$$$$Welcome to the peak!!!!!$$$$$$$$$$$'
        #     print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        #     print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        #     print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        #     print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
        #     print '$$$$$$$$$$$$$$$$$$$$$$$$$'
        #     w1n = (w2n + w0n) / 2
        #     w1w = (w2w + w0w) / 2
        #     wgp1 = (wgp2 + wgp0) / 2
        #     c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz) #!!!!!!!!!!!!!!!!!!!redundant. It can be replaced by c12b
        #     while abs(c01) < c1 or abs(c01) > c2: # If this is not our search element
        #         # If the current middle element is less than x then move the left next to mid
        #         # Else we move right next to mid
        #         if i > 10:
        #             break
        #         if  abs(c01) < c1:
        #             left = mid + 1
        #         if  abs(c01) > c2:
        #             right = mid - 1
        #         mid = (right + left)/2
        #         w2n = w2n_arr[mid]
        #         w2w = w2w_arr[mid]
        #         wgp2 = wgp2_arr[mid]
        #         print '$$$$$$$$$$$La Gauche side of the one slice graph$$$$$$$$$$$$$$'
        #         print '$$$$$$$$$$$$$$$$$$$$$$$$$'
        #         w1n = (w2n + w0n) / 2
        #         w1w = (w2w + w0w) / 2
        #         wgp1 = (wgp2 + wgp0) / 2
        #         c01, c02, c12 = coupling_co(w0n, w0w, w1n, w1w, w2n, w2w, wgp0, wgp1, wgp2, dz)
        #         i+=1
        #     print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1n
        #     print '&&&&&&&&&&&&&& Width Found!!! &&&&&&&&&&&&&& ',w1w
        
    return w1n, w1w, w2n, w2w, wgp1, wgp2, c01, c02, c12

def stripe_w(w0,w2): ### Afunction to get the stripe width of a linear taper
    wtot = 6e-6
    ycentre = wtot/2  ###ridge centre y location
    
    yleft0 = ycentre - w0/2
    yright0 = ycentre + w0/2
    yleft2 = ycentre - w2/2
    yright2 = ycentre + w2/2
    
    wsl = yleft2 - yleft0 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    return wsl, wsr
    
def max_w2 (w0,limit):  #### getting the maximum finish width of a slice so that the stripe width is greater than a limit
    w2_arr = np.linspace(w0,0,5000)
    for i in range(0, len(w2_arr)):
        wsl, wsr = stripe_w(w0,w2_arr[i])
        if (wsl >= limit and wsr >= limit):
            w2 = w2_arr[i]
            break
    return w2
    
def coupling_co(wcn0, wcw0,wcn1, wcw1, wcn2, wcw2, wgp0, wgp1, wgp2,dz): ###a function to calculate the coupling coefficient of a linear taper
    print '===================================================='
    ##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
    wtot = 6e-6
    ycentre = wtot/2  ###ridge centre y location
    xsin = t_substrate + t_core/2  ###ridge centre x location of wg

    yr1 = ycentre + 0.5 * wgp1
    yl1 = ycentre - 0.5 * wgp1
    yr0 = ycentre + 0.5 * wgp0
    yl0 = ycentre - 0.5 * wgp0
    yr2 = ycentre + 0.5 * wgp2
    yl2 = ycentre - 0.5 * wgp2
    
    yleft1 = yl1 - wcn1
    yright1 = yr1 + wcw1
    yleft0 = yl0 - wcn0
    yright0 = yr0 + wcw0
    yleft2 = yl2 - wcn2
    yright2 = yr2 + wcw2
    
    wsl = yleft0 - yleft2 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    wsml = yl0 - yl2
    wsmr = yr2 - yr0
    stripe_t = t_core ###Stripes height
    print 'left stripe width =', abs(wsl)
    print 'right stripe width =', abs(wsr)
    print 'middle left stripe width =', abs(wsml)
    print 'middle right stripe width =', abs(wsmr)
    #############################################################################
    
    
    ##### 3 - Define wg_1
    s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
    s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
    rwg_wire = reme.RWG(s_clad(yleft1) + s_core(wcn1) + s_clad(wgp1) + s_core(wcw1) + s_clad(wtot-yright1))
    guide = reme.FMMStraight(rwg_wire)
    guide.set_matching_point(4.3)
    guide.set_left_boundary(reme.PEC)
    guide.set_right_boundary(reme.PEC)
    guide.set_top_boundary(reme.PEC)
    guide.set_bottom_boundary(reme.PEC)
    guide.set_degenerate(True)
    guide.polish_mode_list()
    guide.find_modes(5, m_si.n().real, m_sio2.n().real, scan_step=1e-3)

    # #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% making sure modes are correct. from previous simulations some modes get missed or calculated incorrectly
    # neff0 = guide.get_mode_effective_index(0).real
    # neff1 = guide.get_mode_effective_index(1).real
    # match = 3.9
    # while((neff0 < 2.5 or neff1 < 2.4) and match <= 4.9):
    #     print '%%%%%%%%%%%%%%%%%%%%%%%%wtfwtfwf%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
    #     match += 0.1
    #     guide.set_matching_point(match)
    #     guide.set_left_boundary(reme.PEC)
    #     guide.set_right_boundary(reme.PEC)
    #     guide.set_top_boundary(reme.PEC)
    #     guide.set_bottom_boundary(reme.PEC)
    #     guide.set_degenerate(True)
    #     guide.polish_mode_list()
    #     guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    #     neff0 = guide.get_mode_effective_index(0).real
    #     neff1 = guide.get_mode_effective_index(1).real
    #
    #
    # match = 1.9
    # while ((neff0 < 2.5 or neff1 < 2.4) and match <= 2.9):
    #     print '$$$$$$$$$$$$$$$wtfwtfwf$$$$$$$$$$$$$$$$$$$$$$$$$$'
    #     match += 0.1
    #     guide.set_matching_point(match)
    #     guide.set_left_boundary(reme.PEC)
    #     guide.set_right_boundary(reme.PEC)
    #     guide.set_top_boundary(reme.PEC)
    #     guide.set_bottom_boundary(reme.PEC)
    #     guide.set_degenerate(True)
    #     guide.polish_mode_list()
    #     guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    #     neff0 = guide.get_mode_effective_index(0).real
    #     neff1 = guide.get_mode_effective_index(1).real
    # #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ########################
    
    ####### 4 - Defining wg_0 and wg_2

    
    wg_0 = reme.RWG(s_clad(yleft0) + s_core(wcn0) + s_clad(wgp0) + s_core(wcw0) + s_clad(wtot - yright0))
    wg_2 = reme.RWG(s_clad(yleft2) + s_core(wcn2) + s_clad(wgp0) + s_core(wcw2) + s_clad(wtot - yright2))

    #####################################

    ####### 5 - Getting n0 and n2, and calculating dn2/dz
    nxx = 100
    nyy = 100
    n0l = 2.0 ###SiN refractive index
    n2l = 3.455 ###Si refractive index
    dn2_dz = (n2l ** 2 - n0l ** 2) / (2 * dz)
    print 'dn2/dz =', dn2_dz

    ###################################################################
    
    ######## 8 - Calculating betas and defining constants
    k0 = 2.0*np.pi/wavelength
    epsilon0 =  8.854187817e-12
    mu0 = 4.0e-7*np.pi
    beta0 = k0*guide.get_mode_effective_index(0).real
    beta1 = k0*guide.get_mode_effective_index(1).real
    beta2 = k0*guide.get_mode_effective_index(2).real
    dx = stripe_t/(nxx-1)
    dy = wsl/(nyy-1)
    ################################################
    
    ####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
    xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    xmleft = xmright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    if yleft0 > yleft2:
        yleft = np.linspace(yleft2, yleft0, nyy)
        dn2_dz_arrayl = abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        yleft = np.linspace(yleft0, yleft2, nyy)
        dn2_dz_arrayl = -1*abs(dn2_dz) * np.ones((nxx, nyy))

    if yright2 > yright0:
        yright = np.linspace(yright0, yright2, nyy)
        dn2_dz_arrayr = abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        yright = np.linspace(yright2, yright0, nyy)
        dn2_dz_arrayr = -1*abs(dn2_dz) * np.ones((nxx, nyy))

    if yl0 > yl2:
        ymleft = np.linspace(yl2, yl0, nyy)
        dn2_dz_arrayml = -1*abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        ymleft = np.linspace(yl0, yl2, nyy)
        dn2_dz_arrayml = abs(dn2_dz) * np.ones((nxx, nyy))

    if yr2 > yr0:
        ymright = np.linspace(yr0, yr2, nyy)
        dn2_dz_arraymr = -1*abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        ymright = np.linspace(yr2, yr0, nyy)
        dn2_dz_arraymr = abs(dn2_dz) * np.ones((nxx, nyy))

    e0_leftx = np.zeros((nxx, nyy), complex)
    e1_leftx = np.zeros((nxx, nyy), complex)
    e2_leftx = np.zeros((nxx, nyy), complex)
    e0_lefty = np.zeros((nxx, nyy), complex)
    e1_lefty = np.zeros((nxx, nyy), complex)
    e2_lefty = np.zeros((nxx, nyy), complex)
    e0_leftz = np.zeros((nxx, nyy), complex)
    e1_leftz = np.zeros((nxx, nyy), complex)
    e2_leftz = np.zeros((nxx, nyy), complex)
    e0_rightx = np.zeros((nxx, nyy), complex)
    e1_rightx = np.zeros((nxx, nyy), complex)
    e2_rightx = np.zeros((nxx, nyy), complex)
    e0_righty = np.zeros((nxx, nyy), complex)
    e1_righty = np.zeros((nxx, nyy), complex)
    e2_righty = np.zeros((nxx, nyy), complex)
    e0_rightz = np.zeros((nxx, nyy), complex)
    e1_rightz = np.zeros((nxx, nyy), complex)
    e2_rightz = np.zeros((nxx, nyy), complex)

    e0_mleftx = np.zeros((nxx, nyy), complex)
    e1_mleftx = np.zeros((nxx, nyy), complex)
    e2_mleftx = np.zeros((nxx, nyy), complex)
    e0_mlefty = np.zeros((nxx, nyy), complex)
    e1_mlefty = np.zeros((nxx, nyy), complex)
    e2_mlefty = np.zeros((nxx, nyy), complex)
    e0_mleftz = np.zeros((nxx, nyy), complex)
    e1_mleftz = np.zeros((nxx, nyy), complex)
    e2_mleftz = np.zeros((nxx, nyy), complex)
    e0_mrightx = np.zeros((nxx, nyy), complex)
    e1_mrightx = np.zeros((nxx, nyy), complex)
    e2_mrightx = np.zeros((nxx, nyy), complex)
    e0_mrighty = np.zeros((nxx, nyy), complex)
    e1_mrighty = np.zeros((nxx, nyy), complex)
    e2_mrighty = np.zeros((nxx, nyy), complex)
    e0_mrightz = np.zeros((nxx, nyy), complex)
    e1_mrightz = np.zeros((nxx, nyy), complex)
    e2_mrightz = np.zeros((nxx, nyy), complex)

    for i in range(0, nxx):
        for j in range(0, nyy):
            ####field at left stripe
            e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
            e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
            e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
            e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
            e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
            e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
            e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
            e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
            e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
            ####field at right stripe
            e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
            e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
            e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
            e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
            e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
            e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
            e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
            e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
            e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z

            ####field at middle left stripe
            e0_mleftx[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).x
            e1_mleftx[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).x
            e2_mleftx[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).x
            e0_mlefty[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).y
            e1_mlefty[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).y
            e2_mlefty[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).y
            e0_mleftz[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).z
            e1_mleftz[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).z
            e2_mleftz[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).z
            ####field at middle right stripe
            e0_mrightx[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).x
            e1_mrightx[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).x
            e2_mrightx[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).x
            e0_mrighty[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).y
            e1_mrighty[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).y
            e2_mrighty[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).y
            e0_mrightz[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).z
            e1_mrightz[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).z
            e2_mrightz[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).z
    ##############################################################################################

    ####### 10 - Calculating coupling coefficient
    c01l = 0.0
    c02l = 0.0
    c12l = 0.0
    c01r = 0.0
    c02r = 0.0
    c12r = 0.0

    c01ml = 0.0
    c02ml = 0.0
    c12ml = 0.0
    c01mr = 0.0
    c02mr = 0.0
    c12mr = 0.0

    c01 = 0.0
    c02 = 0.0
    c12 = 0.0

    for i in range(nxx):  ###Dot product of the fields at the left and right stripes
        for j in range(nyy):
            c01l += np.conjugate(e0_leftx[i][j]) * e1_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j]) * \
                    e1_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j]) * e1_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c02l += np.conjugate(e0_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j]) * \
                    e2_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j]) * e2_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c12l += np.conjugate(e1_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e1_lefty[i][j]) * \
                    e2_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e1_leftz[i][j]) * e2_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c01r += np.conjugate(e0_rightx[i][j]) * e1_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e0_righty[i][j]) * e1_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j]) * e1_rightz[i][
                        j] * dn2_dz_arrayr[i][j]
            c02r += np.conjugate(e0_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e0_righty[i][j]) * e2_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j]) * e2_rightz[i][
                        j] * dn2_dz_arrayr[i][j]
            c12r += np.conjugate(e1_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e1_righty[i][j]) * e2_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e1_rightz[i][j]) * e2_rightz[i][
                        j] * dn2_dz_arrayr[i][j]

            c01ml += np.conjugate(e0_mleftx[i][j]) * e1_mleftx[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(
                e0_mlefty[i][j]) * e1_mlefty[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(e0_mleftz[i][j]) * \
                     e1_mleftz[i][j] * dn2_dz_arrayml[i][j]
            c02ml += np.conjugate(e0_mleftx[i][j]) * e2_mleftx[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(
                e0_mlefty[i][j]) * e2_mlefty[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(e0_mleftz[i][j]) * \
                     e2_mleftz[i][j] * dn2_dz_arrayml[i][j]
            c12ml += np.conjugate(e1_mleftx[i][j]) * e2_mleftx[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(
                e1_mlefty[i][j]) * e2_mlefty[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(e1_mleftz[i][j]) * \
                     e2_mleftz[i][j] * dn2_dz_arrayml[i][j]
            c01mr += np.conjugate(e0_mrightx[i][j]) * e1_mrightx[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(
                e0_mrighty[i][j]) * e1_mrighty[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(e0_mrightz[i][j]) * \
                     e1_mrightz[i][j] * dn2_dz_arraymr[i][j]
            c02mr += np.conjugate(e0_mrightx[i][j]) * e2_mrightx[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(
                e0_mrighty[i][j]) * e2_mrighty[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(e0_mrightz[i][j]) * \
                     e2_mrightz[i][j] * dn2_dz_arraymr[i][j]
            c12mr += np.conjugate(e1_mrightx[i][j]) * e2_mrightx[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(
                e1_mrighty[i][j]) * e2_mrighty[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(e1_mrightz[i][j]) * \
                     e2_mrightz[i][j] * dn2_dz_arraymr[i][j]

    c01 = c01l + c01r + c01ml + c01mr
    c02 = c02l + c02r + c02ml + c02mr
    c12 = c12l + c12r + c12ml + c12mr

    c01 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta1) * c01 * (dx * dy)
    c02 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta2) * c02 * (dx * dy)
    c12 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta1 - beta2) * c12 * (dx * dy)

    print (c01)
    print (c02)
    print (c12)
    
    return c01,c02,c12


#========================================================= Nonlinear taper construction =============================================================================
set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding

c01_array = []
c02_array = []
c12_array = []
wn_array = []
ww_array = []
wgp_array = []
z_array = []


##==========================================Algorithm for nonlinear taper optimization====================================#
##### 1 - Define dimensions of first segment

w00 = 0.43e-6
w0 = 0.54e-6
w3 = 0.86e-6
w4 = 0.54e-6
wg1 = 0.15e-6
wg2 = 2e-6
wtot = 6e-6
dz = 0.5e-6
limit = 3e-9  ###minimum limit on strip width
w2w_arr = np.linspace(w3,w4-0.01e-6,100000)  ###finish width of each slice
w2n_arr = np.linspace(w00,w0+0.01e-6,100000)
wgp2_arr = np.linspace(wg1,wg2+0.05e-6,100000)
w0n = w00
w0w = w3
wgp0 = wg1
########################################################


#### 5 - Doing Bisection search along the taper until the finish width of the taper is equal to we
n = 0  ### number of 2dz's
c1 = 31980 #### lower bound for coupling coefficient
c2 = 32000 #### higher bound for coupling coefficient
while (wgp0 <= wg2):
    z_array.append(n*2*dz)
    ##### 2 - Calculate coupling coefficients
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0
    w1n = 0.0
    w1n = 0.0
    w2w = 0.0
    w2w = 0.0
    wgp1 = 0.0
    wgp2 = 0.0
    
    w1n, w1w, w2n, w2w, wgp1, wgp2, c01, c02, c12 = golden_search_output(c1, c2, w0n, w0w, wgp0, w2n_arr, w2w_arr, wgp2_arr, dz, n)   ###Searching for w1 that satisfy c12 in the range c1 to c2
#    print w2
    wn_array.append(w1n)
    ww_array.append(w1w)
    wgp_array.append(wgp1)
    c01_array.append(c01)
    c02_array.append(c02)
    c12_array.append(c12)
    
    w0n = w2n  ####setting start width of next slice equals to the finish width of current slice
    w0w = w2w
    wgp0 = wgp2
    n += 1  ###getting the number of 2dz's in order to get the total length of the taper
#####################################################################################################
##========================================================================================================================#

######Converting the arrays to fixed sized arrays to allow multiplication with float
z = np.zeros(len(z_array))
wn = np.zeros(len(wn_array))
ww = np.zeros(len(wn_array))
wgp = np.zeros(len(wgp_array))
c01 = np.zeros(len(c01_array))
c02 = np.zeros(len(c02_array))
c12 = np.zeros(len(c12_array))

for i in range (0, len(z)):
    z[i] = z_array[i]*1e6
    wn[i] = wn_array[i]*1e6
    ww[i] = ww_array[i]*1e6
    wgp[i] = wgp_array[i] * 1e6
    c01[i] = c01_array[i]
    c02[i] = c02_array[i]
    c12[i] = c12_array[i]
###################################################################################


#writing to files

with open('PBSR_SiN_adiabatic_outputregion_compact_z', 'wb') as f:
    pickle.dump(z, f)
    
with open('PBSR_SiN_adiabatic_outputregion_compact_wn', 'wb') as f:
    pickle.dump(wn, f)
    
with open('PBSR_SiN_adiabatic_outputregion_compact_ww', 'wb') as f:
    pickle.dump(ww, f)

with open('PBSR_SiN_adiabatic_outputregion_compact_wgp', 'wb') as f:
    pickle.dump(wgp, f)
    
with open('PBSR_SiN_adiabatic_outputregion_compact_c01', 'wb') as f:
    pickle.dump(c01, f)
    
with open('PBSR_SiN_adiabatic_outputregion_compact_c02', 'wb') as f:
    pickle.dump(c02, f)
    
with open('PBSR_SiN_adiabatic_outputregion_compact_c12', 'wb') as f:
    pickle.dump(c12, f)

######Plotting w, c01, c02 and c12
plt.plot(z, wn, 'r')
plt.xlabel('z (um)')
plt.ylabel('narrow waveguide (um)')
plt.show()

plt.plot(z, ww, 'b')
plt.xlabel('z (um)')
plt.ylabel('wide waveguide(um)')
plt.show()

plt.plot(z, wgp, 'g')
plt.xlabel('z (um)')
plt.ylabel('Gap Width(um)')
plt.show()

plt.plot(z, c01, 'r', z, c02, 'b', z, c12, 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.show()
    
plt.plot(z, abs(c01), 'r', z, abs(c02), 'b', z, abs(c12), 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.show()
##################################################################################
# plt.plot(z, abs(c01), 'r')
# plt.xlabel('z (um)')
# plt.ylabel('c01')
# plt.show()
##reading from files
#    
#with open('LNOI_PBSR_adiabatic_taper_z', 'rb') as f:
#    z_file = pickle.load(f)
#
#with open('LNOI_PBSR_adiabatic_taper_w', 'rb') as f:
#    w_file = pickle.load(f)
#
#with open('LNOI_PBSR_adiabatic_taper_c01', 'rb') as f:
#    c01_file = pickle.load(f)
#    
#with open('LNOI_PBSR_adiabatic_taper_c02', 'rb') as f:
#    c02_file = pickle.load(f)
#    
#with open('LNOI_PBSR_adiabatic_taper_c12', 'rb') as f:
#    c12_file = pickle.load(f)