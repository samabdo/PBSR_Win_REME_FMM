####################################################################
#
# Repeating Dai et al 2011 ltp1 and ltp3 included
#
####################################################################

from reme import *
import time as time

set_wavelength(1.55e-6)
set_num_slab_modes(30)
#set_max_waveguide_index(2.35)
set_fmm_scan_step(1e-3)
set_samplings(101, 401)
set_numerical_integration(True)
set_save_memory(True)

# Define parameters: waveguide width, gap, ring radius
w0 = 0.54e-6
w1 = 0.69e-6
w2 = 0.83e-6
w3 = 0.9e-6

#lwg0 = 4e-6
ltp1 = 4e-6
ltp2 = 44e-6
ltp3 = ltp1*(w3-w2)/(w1-w0)
#ldc = 7e-6
lt = ltp1 + ltp2 + ltp3

# Simulation window
length = 44e-6
height = 2e-6


m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6       # thickness of the SiO2 substrate
t_core = 220e-9          # thickness of the core
t_cladding = 1e-6        # thickness of the top cladding

xcut = t_substrate + t_core/2

#device in x direction (vertical)
core = Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
clad = Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))



#guide0 = Device()
taper1 = Device()
taper2 = Device()
taper3 = Device()
#guide4 = Device()

steps = 50
#dz0 = lwg0/steps;
dz1 = ltp1/steps;
dz2 = ltp2/steps;
dz3 = ltp3/steps;
#dz4 = ldc/steps;

wgs = list()
#wgs1 = list()
#wgs2 = list()
#wgs3 = list()
#wgs4 = list()


for i in range (0, steps+1, 1):
    

    ####taper 1
    
    z1 = dz1*i
    y2 = (w1-w0)*z1/(2*ltp1) + w0/2 + 3e-6
    y1 = -(w1-w0)*z1/(2*ltp1) - w0/2 + 3e-6

    wg1 = FMMStraight(3)

    wg1.set_slab(1, clad, y1)
    wg1.set_slab(2, core, y2-y1)
    wg1.set_slab(3, clad, y2)
    
    wg1.set_left_boundary(PEC)
    wg1.set_right_boundary(PEC)
    wg1.set_degenerate(True)

    wgs.append(wg1)
    taper1.add_waveguide(wg1, dz1)
    
    ####taper 2
    
    z2 = dz2*i
    y4 = (w2-w1)*z2/(2*ltp2) + w1/2 + 3e-6
    y3 = -(w2-w1)*z2/(2*ltp2) - w1/2 + 3e-6

    wg2 = FMMStraight(3)

    wg2.set_slab(1, clad, y3)
    wg2.set_slab(2, core, y4-y3)
    wg2.set_slab(3, clad, y4)
    
    wg2.set_left_boundary(PEC)
    wg2.set_right_boundary(PEC)
    wg2.set_degenerate(True)

    wgs.append(wg2)
    taper2.add_waveguide(wg2, dz2)
    
    
    ####taper 3
    
    z3 = dz3*i
    y6 = (w3-w2)*z3/(2*ltp3) + w2/2 + 3e-6
    y5 = -(w3-w2)*z3/(2*ltp3) - w2/2 + 3e-6

    wg3 = FMMStraight(3)

    wg3.set_slab(1, clad, y5)
    wg3.set_slab(2, core, y6-y5)
    wg3.set_slab(3, clad, y6)
    
    wg3.set_left_boundary(PEC)
    wg3.set_right_boundary(PEC)
    wg3.set_degenerate(True)

    wgs.append(wg3)
    taper3.add_waveguide(wg3, dz3)




# Build the whole taper
taper = Device([taper1, taper2, taper3])

# View the refractive index distribution
#taper.view_refractive_index_Xcut(xcut, 1e-6, 3e-6, 1000, 0, lt, 1000)



# Excite the device with TM0 mode, amplitude 1.0
taper.set_incident_left(1, 1.0)

taper.calculate()

#print "Refelected power ", abs(taper.get_left_output_port(0, 0))**2
#print "Output power: ", abs(taper.get_right_output_port(0, 0))**2

# View the field
taper.view_field_Xcut('E', xcut, 2e-6, 4e-6, 1000, 0, lt, 1000)

#taper.view_field_Zcut('E', 0, 0, 2.22e-6, 1000, 1.5e-6, 2.5e-6, 1000)
#taper.view_field_Zcut('E', lt, 0, 2.22e-6, 1000, 1.5e-6, 2.5e-6, 1000)

# View the waveguide mode evolution
taper.view_mode_evolution()

# Get amplitude of output mode
print("Output TE1 mode amplitude: {}".format(abs(taper.get_output_right(1))))
print("Output TM0 mode amplitude: {}".format(abs(taper.get_output_right(2))))

# You can view the mode field of diffent waveguide sections

# wgs[0].view_modes()
# wgs[steps/2].view_modes()
# wgs[steps-1].view_modes()
