#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays
import pickle


#========================================================= Define waveguide parameters =============================================================================
set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding


##### 1 - Define parameters: taper widths, gap and total width. Also defining coupling coefficient array
wtot = 5e-6
w0 = 0.54e-6
w1 = 0.69e-6
w2 = 0.83e-6
w3 = 0.9e-6

#lwg0 = 4e-6
ltp1 = 4e-6
ltp2 = 44e-6
ltp3 = ltp1*(w3-w2)/(w1-w0)
#ldc = 7e-6
lt = ltp1 + ltp2 + ltp3

dz = 0.5e-6

steps1 = ltp1/dz
steps2 = ltp2/dz
steps3 = ltp3/dz

dw1 = (w1-w0)/steps1;
dw2 = (w2-w1)/steps2;
dw3 = (w3-w2)/steps3;

ws = np.linspace(w0, w1, steps1)
wm = np.linspace(w1 + dw2, w2, steps2)
we = np.linspace(w2 + dw3, w3, steps3)
#steps = steps1 + steps2 + steps3

zs = np.linspace(0, ltp1, steps1)
zm = np.linspace(ltp1 + dz, ltp1+ltp2, steps2)
ze = np.linspace(ltp1+ltp2 + dz, lt, steps3)

w = np.hstack((ws,wm,we))
z = np.hstack((zs,zm,ze))


plt.plot(z*1e6, w*1e6)
plt.xlabel('z (um)')
plt.ylabel('width (um)')
plt.show()

c01_array = np.zeros(len(z),complex)
c02_array = np.zeros(len(z),complex)
c12_array = np.zeros(len(z),complex)
########################################################


for l in range (1, len(z) - 1, 1):
    print ("#####################################", l, "#########################################")
    print ("################################ z = ", z[l]*1e6, "######################################")
    ##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
    ycentre = wtot/2  ###ridge centre y location
    w_clad = (wtot - w[l]) / 2
    xsin = t_substrate + t_core/2  ###ridge centre x location of wg

    yleft1 = ycentre - w[l]/2
    yright1 = ycentre + w[l]/2
    yleft2 = ycentre - w[l-1]/2
    yright2 = ycentre + w[l-1]/2
    yleft0 = ycentre - w[l+1]/2
    yright0 = ycentre + w[l+1]/2

    wsl = yleft2 - yleft0 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    stripe_t = t_core ###Stripes height
    print 'left stripe width =', wsl
    print 'right stripe width =', wsr
    #############################################################################


    ##### 3 - Define wg_1
    s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
    s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
    rwg_wire = reme.RWG(s_clad(w_clad) + s_core(w[l]) + s_clad(w_clad))

    #wg.view(aspect_ratio_equal=False) ###View the waveguide structure
    ########################################################################################

    ####### 4 - Defining wg_0 and wg_2

    wg_0 = reme.RWG(s_clad((wtot - w[l-1])/2) + s_core(w[l-1]) + s_clad((wtot - w[l-1])/2))
    wg_2 = reme.RWG(s_clad((wtot - w[l+1])/2) + s_core(w[l+1]) + s_clad((wtot - w[l+1])/2))
    #####################################

    ####### 5 - Getting n0 and n2
    n0 = wg_0.get_refractive_index(xsin, yleft1).real
    print 'n0 =',n0
    n2 = wg_2.get_refractive_index(xsin, yleft1).real
    print 'n2 =',n2
    #################################

    ####### 6 - Calculating dn2/dz and constructing an array for dn2/dz

    dn = n0 - n2
    print 'dn =', dn
    dn2_dz = (n2**2 - n0**2)/(2*dz)
    print 'dn2/dz =', dn2_dz
    nxx = 100
    nyy = 100
    dn2_dz_array = dn2_dz*np.ones((nxx,nyy))
    ###################################################################

    ######### 7 - Using FEM solver and finding modes
    straight_wire = reme.FMMStraight(rwg_wire)
    straight_wire.set_left_boundary(reme.PEC)
    straight_wire.set_right_boundary(reme.PEC)
    straight_wire.set_top_boundary(reme.PEC)
    straight_wire.set_bottom_boundary(reme.PEC)

    straight_wire.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    #################################################

    ######## 8 - Calculating betas and defining constants
    k0 = 2.0*np.pi/wavelength
    epsilon0 =  8.854187817e-12
    mu0 = 4.0e-7*np.pi
    beta0 = k0 * straight_wire.get_mode_effective_index(0).real
    beta1 = k0 * straight_wire.get_mode_effective_index(1).real
    beta2 = k0 * straight_wire.get_mode_effective_index(2).real
    dx = stripe_t/(nxx-1)
    dy = wsl/(nyy-1)
    ################################################

    ####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
    xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    yleft = np.linspace(yleft0, yleft2, nyy)
    yright = np.linspace(yright2, yright0, nyy)

    e0_leftx = np.zeros((nxx, nyy), complex)
    e1_leftx = np.zeros((nxx, nyy), complex)
    e2_leftx = np.zeros((nxx, nyy), complex)
    e0_lefty = np.zeros((nxx, nyy), complex)
    e1_lefty = np.zeros((nxx, nyy), complex)
    e2_lefty = np.zeros((nxx, nyy), complex)
    e0_leftz = np.zeros((nxx, nyy), complex)
    e1_leftz = np.zeros((nxx, nyy), complex)
    e2_leftz = np.zeros((nxx, nyy), complex)
    e0_rightx = np.zeros((nxx, nyy), complex)
    e1_rightx = np.zeros((nxx, nyy), complex)
    e2_rightx = np.zeros((nxx, nyy), complex)
    e0_righty = np.zeros((nxx, nyy), complex)
    e1_righty = np.zeros((nxx, nyy), complex)
    e2_righty = np.zeros((nxx, nyy), complex)
    e0_rightz = np.zeros((nxx, nyy), complex)
    e1_rightz = np.zeros((nxx, nyy), complex)
    e2_rightz = np.zeros((nxx, nyy), complex)

    for i in range(0, nxx):
        for j in range(0, nyy):
            ####field at left stripe
            e0_leftx[i][j] = straight_wire.get_E_field(0, xleft[i], yleft[j]).x
            e1_leftx[i][j] = straight_wire.get_E_field(1, xleft[i], yleft[j]).x
            e2_leftx[i][j] = straight_wire.get_E_field(2, xleft[i], yleft[j]).x
            e0_lefty[i][j] = straight_wire.get_E_field(0, xleft[i], yleft[j]).y
            e1_lefty[i][j] = straight_wire.get_E_field(1, xleft[i], yleft[j]).y
            e2_lefty[i][j] = straight_wire.get_E_field(2, xleft[i], yleft[j]).y
            e0_leftz[i][j] = straight_wire.get_E_field(0, xleft[i], yleft[j]).z
            e1_leftz[i][j] = straight_wire.get_E_field(1, xleft[i], yleft[j]).z
            e2_leftz[i][j] = straight_wire.get_E_field(2, xleft[i], yleft[j]).z
            ####field at right stripe
            e0_rightx[i][j] = straight_wire.get_E_field(0, xright[i], yright[j]).x
            e1_rightx[i][j] = straight_wire.get_E_field(1, xright[i], yright[j]).x
            e2_rightx[i][j] = straight_wire.get_E_field(2, xright[i], yright[j]).x
            e0_righty[i][j] = straight_wire.get_E_field(0, xright[i], yright[j]).y
            e1_righty[i][j] = straight_wire.get_E_field(1, xright[i], yright[j]).y
            e2_righty[i][j] = straight_wire.get_E_field(2, xright[i], yright[j]).y
            e0_rightz[i][j] = straight_wire.get_E_field(0, xright[i], yright[j]).z
            e1_rightz[i][j] = straight_wire.get_E_field(1, xright[i], yright[j]).z
            e2_rightz[i][j] = straight_wire.get_E_field(2, xright[i], yright[j]).z
    ##############################################################################################

    ####### 10 - Calculating coupling coefficient
    c01l = 0.0
    c02l = 0.0
    c12l = 0.0
    c01r = 0.0
    c02r = 0.0
    c12r = 0.0
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0

    for i in range(nxx): ###Dot product of the fields at the left and right stripes
            for j in range(nyy):
                c01l += np.conjugate(e0_leftx[i][j])*e1_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j])*e1_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j])*e1_leftz[i][j]*dn2_dz_array[i][j]
                c02l += np.conjugate(e0_leftx[i][j])*e2_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j])*e2_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j])*e2_leftz[i][j]*dn2_dz_array[i][j]
                c12l += np.conjugate(e1_leftx[i][j])*e2_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_lefty[i][j])*e2_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_leftz[i][j])*e2_leftz[i][j]*dn2_dz_array[i][j]
                c01r += np.conjugate(e0_rightx[i][j])*e1_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j])*e1_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j])*e1_rightz[i][j]*dn2_dz_array[i][j]
                c02r += np.conjugate(e0_rightx[i][j])*e2_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j])*e2_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j])*e2_rightz[i][j]*dn2_dz_array[i][j]
                c12r += np.conjugate(e1_rightx[i][j])*e2_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_righty[i][j])*e2_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_rightz[i][j])*e2_rightz[i][j]*dn2_dz_array[i][j]

    c01 = c01l + c01r
    c02 = c02l + c02r
    c12 = c12l + c12r

    c01 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta1)*c01*(dx*dy)
    c02 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta2)*c02*(dx*dy)
    c12 = (epsilon0/mu0)**0.5*k0/4.0/(beta1-beta2)*c12*(dx*dy)

    print (c01)
    print (c02)
    print (c12)

    c01_array[l] = c01
    c02_array[l] = c02
    c12_array[l] = c12

print("Loop Done")

c01_array[0]=c01_array[1]
c02_array[0]=c02_array[1]
c12_array[0]=c12_array[1]

c01_array[len(z)-1]= c01_array[len(z)-2]
c02_array[len(z)-1]= c02_array[len(z)-2]
c12_array[len(z)-1]= c12_array[len(z)-2]

plt.plot(z*1e6, c01_array, 'r', z*1e6, c02_array, 'b', z*1e6, c12_array, 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.show()

plt.plot(z*1e6, abs(c01_array), 'r', z*1e6, abs(c02_array), 'b', z*1e6, abs(c12_array), 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.show()