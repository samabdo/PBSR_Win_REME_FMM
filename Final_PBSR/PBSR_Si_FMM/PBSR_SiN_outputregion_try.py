#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays

#========================================================= Define waveguide parameters =============================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt


#========================================================= Define waveguide parameters =============================================================================


set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)
# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding


# Define parameters: waveguide width, gap, ring radius
w00 = 0.43e-6
w0 = 0.54e-6
w3 = 0.86e-6
w4 = 0.54e-6
wgp1 = 0.15e-6
wgp2 = 2e-6
wtot = 6e-6

dz = 2e-6
length = 10e-6
steps = int (round(length/dz))
z = np.linspace(0, length, steps)
#w = np.linspace(w3, w4, steps)

wcn = np.linspace(w00, w0, steps)
wcw = np.linspace(w3, w4, steps)
wgp = np.linspace (wgp1, wgp2, steps)

########################################################

##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
ycentre = wtot/2  ###ridge centre y location
xsin = t_substrate + t_core/2  ###ridge centre x location of wg

yr1 = ycentre + 0.5*wgp[2]
yl1 = ycentre - 0.5*wgp[2]
yr0 = ycentre + 0.5*wgp[1]
yl0 = ycentre - 0.5*wgp[1]
yr2 = ycentre + 0.5*wgp[3]
yl2 = ycentre - 0.5*wgp[3]

yleft1 = yl1 - wcn[2]
yright1 = yr1 + wcw[2]
yleft0 = yl0 - wcn[1]
yright0 = yr0 + wcw[1]
yleft2 = yl2 - wcn[3]
yright2 = yr2 + wcw[3]

wsl = yleft0 - yleft2 ###width of left stripe
wsr = yright2 - yright0 ###width of right stripe
wsml = yl0 - yl2
wsmr = yr2 - yr0
stripe_t = t_core ###Stripes height
print 'left stripe width', wsl
print 'right stripe width =', wsr
print 'middle left stripe width =', wsml
print 'middle right stripe width =', wsmr
#############################################################################


##### 3 - Define wg_1
s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
rwg_wire = reme.RWG(s_clad(yleft1) + s_core(wcn[2]) + s_clad(wgp[2]) + s_core(wcw[2]) + s_clad(wtot - yright1))
# rwg_wire.view()
guide = reme.FMMStraight(rwg_wire)
guide.set_matching_point(4.5)
guide.set_left_boundary(reme.PEC)
guide.set_right_boundary(reme.PEC)
guide.set_top_boundary(reme.PEC)
guide.set_bottom_boundary(reme.PEC)
guide.set_degenerate(True)
guide.polish_mode_list()
# guide.view()
guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
########################################################################################

####### 4 - Defining wg_0 and wg_2
s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))


wg_0 = reme.RWG(s_clad(yleft0) + s_core(wcn[1]) + s_clad(wgp[1]) + s_core(wcw[1]) + s_clad(wtot - yright0))
wg_2 = reme.RWG(s_clad(yleft2) + s_core(wcn[3]) + s_clad(wgp[3]) + s_core(wcw[3]) + s_clad(wtot - yright2))

#####################################

####### 5 - Getting n0 and n2, and calculating dn2/dz
nxx = 100
nyy = 100
n0l = wg_0.get_refractive_index(xsin, yleft1).real
n2l = wg_2.get_refractive_index(xsin, yleft1).real
dn2_dzl = (n2l**2 - n0l**2)/(2*dz)
print 'dn2/dzl =', dn2_dzl
dn2_dz_arrayl = dn2_dzl*np.ones((nxx,nyy))

n0ml = wg_0.get_refractive_index(xsin, yl1).real
n2ml = wg_2.get_refractive_index(xsin, yl1).real
dn2_dzml = (n2ml**2 - n0ml**2)/(2*dz)
print 'dn2/dzml =', dn2_dzml
dn2_dz_arrayml = dn2_dzml*np.ones((nxx,nyy))

n0r = wg_0.get_refractive_index(xsin, yright1).real
n2r = wg_2.get_refractive_index(xsin, yright1).real
dn2_dzr = (n2r**2 - n0r**2)/(2*dz)
print 'dn2/dzr =', dn2_dzr
dn2_dz_arrayr = dn2_dzr*np.ones((nxx,nyy))

n0mr = wg_0.get_refractive_index(xsin, yr1).real
n2mr = wg_2.get_refractive_index(xsin, yr1).real
dn2_dzmr = (n2mr**2 - n0mr**2)/(2*dz)
print 'dn2/dzmr =', dn2_dzmr
dn2_dz_arraymr = dn2_dzmr*np.ones((nxx,nyy))
###################################################################

######## 8 - Calculating betas and defining constants
k0 = 2.0*np.pi/wavelength
epsilon0 =  8.854187817e-12
mu0 = 4.0e-7*np.pi
beta0 = k0*guide.get_mode_effective_index(0).real
beta1 = k0*guide.get_mode_effective_index(1).real
beta2 = k0*guide.get_mode_effective_index(2).real
dx = stripe_t/(nxx-1)
dy = wsl/(nyy-1)
################################################

####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
xmleft = xmright = np.linspace(t_substrate, t_substrate + t_core, nxx)
if yleft0>yleft2:
    yleft = np.linspace(yleft2, yleft0, nyy)
else:
    yleft = np.linspace(yleft0, yleft2, nyy)

if yright2>yright0:
    yright = np.linspace(yright0, yright2, nyy)
else:
    yright = np.linspace(yright2, yright0, nyy)

if yl0>yl2:
    ymleft = np.linspace(yl2, yl0, nyy)
else:
    ymleft = np.linspace(yl0, yl2, nyy)

if yr2>yr0:
    ymright = np.linspace(yr0, yr2, nyy)
else:
    ymright = np.linspace(yr2, yr0, nyy)

e0_leftx = np.zeros((nxx, nyy), complex)
e1_leftx = np.zeros((nxx, nyy), complex)
e2_leftx = np.zeros((nxx, nyy), complex)
e0_lefty = np.zeros((nxx, nyy), complex)
e1_lefty = np.zeros((nxx, nyy), complex)
e2_lefty = np.zeros((nxx, nyy), complex)
e0_leftz = np.zeros((nxx, nyy), complex)
e1_leftz = np.zeros((nxx, nyy), complex)
e2_leftz = np.zeros((nxx, nyy), complex)
e0_rightx = np.zeros((nxx, nyy), complex)
e1_rightx = np.zeros((nxx, nyy), complex)
e2_rightx = np.zeros((nxx, nyy), complex)
e0_righty = np.zeros((nxx, nyy), complex)
e1_righty = np.zeros((nxx, nyy), complex)
e2_righty = np.zeros((nxx, nyy), complex)
e0_rightz = np.zeros((nxx, nyy), complex)
e1_rightz = np.zeros((nxx, nyy), complex)
e2_rightz = np.zeros((nxx, nyy), complex)

e0_mleftx = np.zeros((nxx, nyy), complex)
e1_mleftx = np.zeros((nxx, nyy), complex)
e2_mleftx = np.zeros((nxx, nyy), complex)
e0_mlefty = np.zeros((nxx, nyy), complex)
e1_mlefty = np.zeros((nxx, nyy), complex)
e2_mlefty = np.zeros((nxx, nyy), complex)
e0_mleftz = np.zeros((nxx, nyy), complex)
e1_mleftz = np.zeros((nxx, nyy), complex)
e2_mleftz = np.zeros((nxx, nyy), complex)
e0_mrightx = np.zeros((nxx, nyy), complex)
e1_mrightx = np.zeros((nxx, nyy), complex)
e2_mrightx = np.zeros((nxx, nyy), complex)
e0_mrighty = np.zeros((nxx, nyy), complex)
e1_mrighty = np.zeros((nxx, nyy), complex)
e2_mrighty = np.zeros((nxx, nyy), complex)
e0_mrightz = np.zeros((nxx, nyy), complex)
e1_mrightz = np.zeros((nxx, nyy), complex)
e2_mrightz = np.zeros((nxx, nyy), complex)

for i in range(0, nxx):
        for j in range (0, nyy):
            ####field at left stripe
            e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
            e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
            e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
            e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
            e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
            e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
            e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
            e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
            e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
            ####field at right stripe
            e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
            e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
            e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
            e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
            e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
            e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
            e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
            e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
            e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z

            ####field at middle left stripe
            e0_mleftx[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).x
            e1_mleftx[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).x
            e2_mleftx[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).x
            e0_mlefty[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).y
            e1_mlefty[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).y
            e2_mlefty[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).y
            e0_mleftz[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).z
            e1_mleftz[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).z
            e2_mleftz[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).z
            ####field at middle right stripe
            e0_mrightx[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).x
            e1_mrightx[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).x
            e2_mrightx[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).x
            e0_mrighty[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).y
            e1_mrighty[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).y
            e2_mrighty[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).y
            e0_mrightz[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).z
            e1_mrightz[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).z
            e2_mrightz[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).z
##############################################################################################

####### 10 - Calculating coupling coefficient
c01l = 0.0
c02l = 0.0
c12l = 0.0
c01r = 0.0
c02r = 0.0
c12r = 0.0

c01ml = 0.0
c02ml = 0.0
c12ml = 0.0
c01mr = 0.0
c02mr = 0.0
c12mr = 0.0

c01 = 0.0
c02 = 0.0
c12 = 0.0

for i in range(nxx): ###Dot product of the fields at the left and right stripes
        for j in range(nyy):
            c01l += np.conjugate(e0_leftx[i][j])*e1_leftx[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j])*e1_lefty[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j])*e1_leftz[i][j]*dn2_dz_arrayl[i][j]
            c02l += np.conjugate(e0_leftx[i][j])*e2_leftx[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j])*e2_lefty[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j])*e2_leftz[i][j]*dn2_dz_arrayl[i][j]
            c12l += np.conjugate(e1_leftx[i][j])*e2_leftx[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e1_lefty[i][j])*e2_lefty[i][j]*dn2_dz_arrayl[i][j] + np.conjugate(e1_leftz[i][j])*e2_leftz[i][j]*dn2_dz_arrayl[i][j]
            c01r += np.conjugate(e0_rightx[i][j])*e1_rightx[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_righty[i][j])*e1_righty[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j])*e1_rightz[i][j]*dn2_dz_arrayr[i][j]
            c02r += np.conjugate(e0_rightx[i][j])*e2_rightx[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_righty[i][j])*e2_righty[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j])*e2_rightz[i][j]*dn2_dz_arrayr[i][j]
            c12r += np.conjugate(e1_rightx[i][j])*e2_rightx[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e1_righty[i][j])*e2_righty[i][j]*dn2_dz_arrayr[i][j] + np.conjugate(e1_rightz[i][j])*e2_rightz[i][j]*dn2_dz_arrayr[i][j]

            c01ml += np.conjugate(e0_mleftx[i][j])*e1_mleftx[i][j]*dn2_dz_arrayml[i][j] + np.conjugate(e0_mlefty[i][j])*e1_mlefty[i][j]*dn2_dz_arrayml[i][j] + np.conjugate(e0_mleftz[i][j])*e1_mleftz[i][j]*dn2_dz_arrayml[i][j]
            c02ml += np.conjugate(e0_mleftx[i][j])*e2_mleftx[i][j]*dn2_dz_arrayml[i][j] + np.conjugate(e0_mlefty[i][j])*e2_mlefty[i][j]*dn2_dz_arrayml[i][j] + np.conjugate(e0_mleftz[i][j])*e2_mleftz[i][j]*dn2_dz_arrayml[i][j]
            c12ml += np.conjugate(e1_mleftx[i][j])*e2_mleftx[i][j]*dn2_dz_arrayml[i][j] + np.conjugate(e1_mlefty[i][j])*e2_mlefty[i][j]*dn2_dz_arrayml[i][j] + np.conjugate(e1_mleftz[i][j])*e2_mleftz[i][j]*dn2_dz_arrayml[i][j]
            c01mr += np.conjugate(e0_mrightx[i][j])*e1_mrightx[i][j]*dn2_dz_arraymr[i][j] + np.conjugate(e0_mrighty[i][j])*e1_mrighty[i][j]*dn2_dz_arraymr[i][j] + np.conjugate(e0_mrightz[i][j])*e1_mrightz[i][j]*dn2_dz_arraymr[i][j]
            c02mr += np.conjugate(e0_mrightx[i][j])*e2_mrightx[i][j]*dn2_dz_arraymr[i][j] + np.conjugate(e0_mrighty[i][j])*e2_mrighty[i][j]*dn2_dz_arraymr[i][j] + np.conjugate(e0_mrightz[i][j])*e2_mrightz[i][j]*dn2_dz_arraymr[i][j]
            c12mr += np.conjugate(e1_mrightx[i][j])*e2_mrightx[i][j]*dn2_dz_arraymr[i][j] + np.conjugate(e1_mrighty[i][j])*e2_mrighty[i][j]*dn2_dz_arraymr[i][j] + np.conjugate(e1_mrightz[i][j])*e2_mrightz[i][j]*dn2_dz_arraymr[i][j]


c01 = c01l + c01r + c01ml + c01mr
c02 = c02l + c02r + c02ml + c02mr
c12 = c12l + c12r + c12ml + c12mr

c01 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta1)*c01*(dx*dy)
c02 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta2)*c02*(dx*dy)
c12 = (epsilon0/mu0)**0.5*k0/4.0/(beta1-beta2)*c12*(dx*dy)

print (c01)
print (c02)
print (c12)
