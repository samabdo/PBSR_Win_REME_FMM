#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt


#========================================================= Define waveguide parameters =============================================================================


set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)
nm = 5  ###number of modes to find
# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding


# Define parameters: waveguide width, gap, ring radius
w00 = 0.38e-6
w0 = 0.43e-6
w3 = 0.95e-6
w4 = 0.86e-6
wgp = 0.15e-6

#wtot = 8e-6

wclad = 3e-6


set_num_slab_modes(50)
#set_fmm_scan_step(1e-4)

# Define core and cladding slabs
#s_core = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sin(t_core) + m_sio2(t_cladding))
#s_clad = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sio2(t_core + t_cladding))


s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))


                     
rwg_wire = reme.RWG(s_clad(wclad) + s_core(w00) + s_clad(wgp) + s_core(w3) + s_clad(wclad))

# rwg_wire.view()

#============================================================= Simulation ========================================================================================

straight_wire = reme.FMMStraight(rwg_wire)

# Enclose the waveguide within PEC boundaries
straight_wire.set_left_boundary(reme.PEC)
straight_wire.set_right_boundary(reme.PEC)
straight_wire.set_top_boundary(reme.PEC)
straight_wire.set_bottom_boundary(reme.PEC)
   
num_points = 61
wcn = np.linspace(w00, w0, num_points)
wcw = np.linspace(w3, w4, num_points)
num = np.linspace(0, num_points, num_points)
waveguide_neff = np.zeros((100, num_points))
te_fraction = np.zeros((100, num_points))


for i in range(num_points):
    rwg_wire.set_width(2, wcn[i])
    rwg_wire.set_width(4, wcw[i])
    straight_wire.polish_mode_list()
    straight_wire.find_modes(nm, m_si.n().real, m_sio2.n().real, clear_existing_modes=True, scan_step=0.1e-3)
    # straight_wire.find_modes(nm, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    print "++++++++++++++++Here++++++++++++++++ ", i
    for k in range(nm):
        n = straight_wire.get_mode_effective_index(k).real
        if n > m_sio2.n().real:
            waveguide_neff[k][i] = n
            te_fraction[k][i] = straight_wire.get_TE_fraction(k)
            
#########plotting supermodes
fig, ax1 = plt.subplots()
for k in range(nm):
    ax1.plot(wcn*1e6, waveguide_neff[k])

ax1.set(xlabel='Narrow Waveguide Width (um)', ylabel='Effective Index')
ax1.set_xlim(wcn[0]*1e6, wcn[num_points-1]*1e6)
ax1.set_ylim(2,3)
ax1.tick_params(axis='x')

ax2 = ax1.twiny()  # instantiate a second axes that shares the same y-axis


ax2.set_xlabel('Wide Waveguide Width (um)')  # we already handled the x-label with ax1
for k in range(nm):
    ax2.plot(wcw*1e6, waveguide_neff[k])
ax2.set_xlim(wcw[0]*1e6, wcw[num_points-1]*1e6)
ax2.set_ylim(2,3)
ax2.tick_params(axis='x')

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()
#################################################################

######## Calculating and plotting delta beta
k0 = 2.0*np.pi/wavelength
betas = np.zeros((10, num_points))
betas = k0 * waveguide_neff
dbeta = betas[1] - betas[2]
dbeta2 = betas[0] - betas[1]
fig, ax1 = plt.subplots()

ax1.set_xlabel('Narrow Waveguide Width (um)')
ax1.set_ylabel('Delta Beta')
ax1.plot(wcn*1e6, dbeta2, label = 'TE0 - TE1')
ax1.plot(wcn*1e6, dbeta, label = 'TE1 - TE2')
ax1.set_xlim(wcn[0]*1e6, wcn[num_points-1]*1e6)
ax1.tick_params(axis='x')

ax2 = ax1.twiny()  # instantiate a second axes that shares the same y-axis
ax2.set_xlabel('Wide Waveguide Width (um)')  # we already handled the x-label with ax1
ax2.plot(wcw*1e6, dbeta2, label = 'TE0 - TE1')
ax2.plot(wcw*1e6, dbeta, label = 'TE1 - TE2')
ax2.set_xlim(wcw[0]*1e6, wcw[num_points-1]*1e6)
ax2.tick_params(axis='x')

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.legend(loc = 'upper left')
plt.show()
################################################################


# ##################Looking at the modes
# rwg = reme.RWG(s_clad(wclad) + s_core(wcn[0]) + s_clad(wgp) + s_core(wcw[0]) + s_clad(wclad))
# rwg.view()
# guide = reme.FMMStraight(rwg)
# guide.find_modes(3, m_si.n().real, m_sio2.n().real, clear_existing_modes=True, scan_step=1e-3)
# guide.view_modes()
#
# rwg = reme.RWG(s_clad(wclad) + s_core(wcn[(len(wcn)-1)/2]) + s_clad(wgp) + s_core(wcw[(len(wcn)-1)/2]) + s_clad(wclad))
# rwg.view()
# guide = reme.FMMStraight(rwg)
# guide.find_modes(3, m_si.n().real, m_sio2.n().real, clear_existing_modes=True, scan_step=1e-3)
# guide.view_modes()
#
# rwg = reme.RWG(s_clad(wclad) + s_core(wcn[len(wcn)-1]) + s_clad(wgp) + s_core(wcw[len(wcw)-1]) + s_clad(wclad))
# rwg.view()
# guide = reme.FMMStraight(rwg)
# guide.find_modes(3, m_si.n().real, m_sio2.n().real, clear_existing_modes=True, scan_step=1e-3)
# guide.view_modes()