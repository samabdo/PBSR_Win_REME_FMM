#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Construct a nonlinear taper that supports TE1 and TM0 modes such that coupling between the two modes is maintained below a certain value

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays

def binary_search(c1, c2, w0, w2_arr, dz):  ### bisection search function
    print '~~~~~~~~~~~~~~~~~~~~~SEARCHING~~~~~~~~~~~~~~~~~~~~'
    print '       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        '
    print '              ~~~~~~~~~~~~~~~~~~~~~               '
    print '                   ~~~~~~~~~~~                    '

    iterations = 1
    left = 0 # Determines the starting index of the list we have to search in
    right = len(w2_arr)-1 # Determines the last index of the list we have to search in
    mid = (right + left)/2
    w2 = w2_arr[mid]
    print 'w2',w2
    w1 = (w2+w0)/2
    c01, c02, c12 = coupling_co(w0,w1,w2,dz) 
    while abs(c12) < c1 or abs(c12) > c2: # If this is not our search element
        # If the current middle element is less than x then move the left next to mid
        # Else we move right next to mid
        
        if  abs(c12) < c1:
            left = mid + 1
        if  abs(c12) > c2:
            right = mid - 1
        mid = (right + left)/2
        w2 = w2_arr[mid]
        print 'w2',w2
        w1 = (w2+w0)/2
        c01, c02, c12 = coupling_co(w0,w1,w2,dz)
        iterations += 1
        print 'iterations = ',str(iterations)
    print '================ Width Found!!! ================== ',w1
    return w1, w2, c01, c02, c12

def stripe_w(w0,w2): ### Afunction to get the stripe width of a linear taper
    wtot = 8e-6
    ycentre = wtot/2  ###ridge centre y location
    
    yleft0 = ycentre - w0/2
    yright0 = ycentre + w0/2
    yleft2 = ycentre - w2/2
    yright2 = ycentre + w2/2
    
    wsl = yleft2 - yleft0 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    return wsl, wsr
    
def max_w2 (w0,limit):  #### getting the maximum finish width of a slice so that the stripe width is greater than a limit
    w2_arr = np.linspace(w0,0,5000)
    for i in range(0, len(w2_arr)):
        wsl, wsr = stripe_w(w0,w2_arr[i])
        if (wsl >= limit and wsr >= limit):
            w2 = w2_arr[i]
            break
    return w2
    

def coupling_co(w0,w1,w2,dz): ###a function to calculate the coupling coefficient of a linear taper
    print '===================================================='
    ##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
    wtot = 8e-6
    ycentre = wtot/2  ###ridge centre y location
    xsin = t_substrate + t_ln + t_core/2  ###ridge centre x location of wg
    
    yleft1 = ycentre - w1/2
    yright1 = ycentre + w1/2
    yleft0 = ycentre - w0/2
    yright0 = ycentre + w0/2
    yleft2 = ycentre - w2/2
    yright2 = ycentre + w2/2
    
    wsl = yleft2 - yleft0 ###width of left stripe
    wsr = yright0 - yright2 ###width of right stripe
    stripe_t = t_core ###Stripes height
    print 'left stripe width =', wsl
    print 'right stripe width =', wsr
    #############################################################################
    
    
    ##### 3 - Define polygons, complex waveguide and adding areas of different meshings
    p_sub = Polygon([(0, 0), (0, wtot), (t_substrate, wtot), (t_substrate, 0)])
    p_ln = Polygon([(t_substrate, 0), (t_substrate, wtot), (t_substrate+t_ln, wtot), (t_substrate+t_ln, 0)])
    p_sinc = Polygon([(t_substrate+t_ln, yleft2), (t_substrate+t_ln, yright2), (t_substrate+t_ln+t_core, yright2), (t_substrate+t_ln+t_core, yleft2)]) ####centre SiN ridge width minus half strip width, with regular meshing
    p_sinl = Polygon([(t_substrate+t_ln, yleft1), (t_substrate+t_ln, yleft2), (t_substrate+t_ln+t_core, yleft2), (t_substrate+t_ln+t_core, yleft1)]) ####left SiN ridge width minus half strip width, with fine meshing
    p_sinr = Polygon([(t_substrate+t_ln, yright2), (t_substrate+t_ln, yright1), (t_substrate+t_ln+t_core, yright1), (t_substrate+t_ln+t_core, yright2)]) ####right SiN ridge width minus half strip width, with fine meshing
    p_airl = Polygon([(t_substrate+t_ln, yleft0), (t_substrate+t_ln, yleft1), (t_substrate+t_ln+t_core, yleft1), (t_substrate+t_ln+t_core, yleft0)]) ####left Air rectangle with the the other half of strip width, with fine meshing
    p_airr = Polygon([(t_substrate+t_ln, yright1), (t_substrate+t_ln, yright0), (t_substrate+t_ln+t_core, yright0), (t_substrate+t_ln+t_core, yright1)]) ####right Air rectangle with the the other half of strip width, with fine meshing
    
    wg = ComplexWaveguide(t_substrate + t_ln + t_core + t_cladding, wtot, m_air) ###Define a complex waveguide with height, width and background material
    
    wg.add_area(p_sub, m_sio2)  ###Adding substrate layer, with regular meshing
    wg.add_area(p_ln, m_ln)  ###Adding LN layer, with regular meshing
    wg.add_area(p_sinc, m_sin)  ###Adding SiN layer, with regular meshing
    wg.add_area(p_sinl, m_sin, 0.001e-6)  ###Adding left SiN layer, with fine meshing
    wg.add_area(p_sinr, m_sin, 0.001e-6)  ###Adding right SiN layer, with fine meshing
    wg.add_area(p_airl, m_air, 0.001e-6)  ###Adding left air layer, with fine meshing
    wg.add_area(p_airr, m_air, 0.001e-6)  ###Adding right air layer, with fine meshing
    
    #wg.view(aspect_ratio_equal=False) ###View the waveguide structure
    ########################################################################################
    
    ####### 4 - Defining wg_0 and wg_2
    s_core = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sin(t_core) + m_air(t_cladding))
    s_clad = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_air(t_core + t_cladding))
    
    wg_0 = reme.RWG(s_clad((wtot - w0)/2) + s_core(w0) + s_clad((wtot - w0)/2))
    wg_2 = reme.RWG(s_clad((wtot - w2)/2) + s_core(w2) + s_clad((wtot - w2)/2)) 
    #####################################
    
    ####### 5 - Getting n0 and n2
    n0 = wg_0.get_refractive_index(xsin, yleft1).real
    print 'n0 =',n0    
    n2 = wg_2.get_refractive_index(xsin, yleft1).real
    print 'n2 =',n2
    #################################
    
    ####### 6 - Calculating dn2/dz and constructing an array for dn2/dz
    dn = n0 - n2
    print 'dn =', dn  
    dn2_dz = (n2**2 - n0**2)/(2*dz)
    print 'dn2/dz =', dn2_dz
    nxx = 100
    nyy = 100
    dn2_dz_array = dn2_dz*np.ones((nxx,nyy))
    ###################################################################
    
    ######### 7 - Using FEM solver and finding modes
    guide = reme.FEStraight(wg, 0.05e-6)
    #guide.view()
    guide.find_modes(3)
    #guide.view_mesh()
    #guide.view_modes()
    #################################################
    
    ######## 8 - Calculating betas and defining constants
    k0 = 2.0*np.pi/wavelength
    epsilon0 =  8.854187817e-12
    mu0 = 4.0e-7*np.pi
    beta0 = k0*guide.get_mode_effective_index(0).real
    beta1 = k0*guide.get_mode_effective_index(1).real
    beta2 = k0*guide.get_mode_effective_index(2).real
    dx = stripe_t/(nxx-1)
    dy = wsl/(nyy-1)
    ################################################
    
    ####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
    xleft = xright = np.linspace(t_substrate + t_ln, t_substrate + t_ln + t_core, nxx)
    yleft = np.linspace(yleft0, yleft2, nyy)
    yright = np.linspace(yright2, yright0, nyy)
    
    e0_leftx = np.zeros((nxx, nyy), complex)
    e1_leftx = np.zeros((nxx, nyy), complex)
    e2_leftx = np.zeros((nxx, nyy), complex)
    e0_lefty = np.zeros((nxx, nyy), complex)
    e1_lefty = np.zeros((nxx, nyy), complex)
    e2_lefty = np.zeros((nxx, nyy), complex)
    e0_leftz = np.zeros((nxx, nyy), complex)
    e1_leftz = np.zeros((nxx, nyy), complex)
    e2_leftz = np.zeros((nxx, nyy), complex)
    e0_rightx = np.zeros((nxx, nyy), complex)
    e1_rightx = np.zeros((nxx, nyy), complex)
    e2_rightx = np.zeros((nxx, nyy), complex)
    e0_righty = np.zeros((nxx, nyy), complex)
    e1_righty = np.zeros((nxx, nyy), complex)
    e2_righty = np.zeros((nxx, nyy), complex)
    e0_rightz = np.zeros((nxx, nyy), complex)
    e1_rightz = np.zeros((nxx, nyy), complex)
    e2_rightz = np.zeros((nxx, nyy), complex)
    
    for i in range(0, nxx):
            for j in range (0, nyy):
                ####field at left stripe
                e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
                e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
                e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
                e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
                e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
                e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
                e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
                e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
                e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
                ####field at right stripe
                e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
                e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
                e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
                e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
                e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
                e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
                e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
                e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
                e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z
    ##############################################################################################            
                
    ####### 10 - Calculating coupling coefficient
    c01l = 0.0
    c02l = 0.0
    c12l = 0.0
    c01r = 0.0
    c02r = 0.0
    c12r = 0.0
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0
    
    for i in range(nxx): ###Dot product of the fields at the left and right stripes
            for j in range(nyy):
                c01l += np.conjugate(e0_leftx[i][j])*e1_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j])*e1_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j])*e1_leftz[i][j]*dn2_dz_array[i][j]
                c02l += np.conjugate(e0_leftx[i][j])*e2_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j])*e2_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j])*e2_leftz[i][j]*dn2_dz_array[i][j]
                c12l += np.conjugate(e1_leftx[i][j])*e2_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_lefty[i][j])*e2_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_leftz[i][j])*e2_leftz[i][j]*dn2_dz_array[i][j]
                c01r += np.conjugate(e0_rightx[i][j])*e1_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j])*e1_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j])*e1_rightz[i][j]*dn2_dz_array[i][j]
                c02r += np.conjugate(e0_rightx[i][j])*e2_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j])*e2_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j])*e2_rightz[i][j]*dn2_dz_array[i][j]
                c12r += np.conjugate(e1_rightx[i][j])*e2_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_righty[i][j])*e2_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_rightz[i][j])*e2_rightz[i][j]*dn2_dz_array[i][j]
    
    c01 = c01l + c01r
    c02 = c02l + c02r
    c12 = c12l + c12r
    
    c01 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta1)*c01*(dx*dy)
    c02 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta2)*c02*(dx*dy)
    c12 = (epsilon0/mu0)**0.5*k0/4.0/(beta1-beta2)*c12*(dx*dy)
    
    print (c01)
    print (c02)
    print (c12)
    
    return c01,c02,c12


#========================================================= Nonlinear taper construction =============================================================================
set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_ln = reme.AnisotropicMaterial(nx=2.2111, ny=2.1376, nz=2.2, name="LNOI")
m_sio2 = Material(1.444, 'SiO2')                #value are from http://refractiveindex.info/
m_sin = Material(1.9963, 'SiN')
m_air = Material(1, 'Air')

# Dimensions
t_substrate = 1e-6       # thickness of the SiO2 substrate
t_core = 300e-9          # thickness of the SiN layer
t_ln = 300e-9            # thickness of the LN layer
t_cladding = 1e-6        # thickness of the top cladding
w_core = 2e-6

c01_array = []
c02_array = []
c12_array = []



##==========================================calculating c12 for once slice====================================#
##### 1 - Define dimensions of first segment
ws = 2.7e-6 ###taper start width
we = 1.5e-6 ###taper end width
dz = 20e-6
limit = 3e-9  ###minimum limit on strip width
w0 = ws
w2_max = max_w2(w0,limit) ### the maximim finish width of each slice that satisfies stripe width greater than the limit
print 'w2_max',w2_max
w2_arr = np.linspace(0,w2_max,50)  ###finish width of each slice
c01_arr = np.zeros(len(w2_arr), complex)
c02_arr = np.zeros(len(w2_arr), complex)
c12_arr = np.zeros(len(w2_arr), complex)


for i in range(0,len(w2_arr)): 
    
    ##### 2 - Calculate coupling coefficients
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0
    w1 = 0.0
    w2 = 0.0
    w2 = w2_arr[i]
    w1 = (w0+w2)/2
    print 'w2 = ', w2*1e6
    c01, c02, c12 = coupling_co(w0,w1,w2,dz)
    
    c01_arr[i] = c01 
    c02_arr[i] = c02
    c12_arr[i] = c12
    


##========================================================================================================================#

######Plotting w, c01, c02 and c12
plt.plot(w2_arr*1e6, c01_arr, 'r', w2_arr*1e6, c02_arr, 'b', w2_arr*1e6, c12_arr, 'g')
plt.xlabel('Finish width of one slice (um)')
plt.ylabel('coupling coefficient')
plt.show()
    
plt.plot(w2_arr*1e6, abs(c01_arr), 'r', w2_arr*1e6, abs(c02_arr), 'b', w2_arr*1e6, abs(c12_arr), 'g')
plt.xlabel('Finish width of one slice (um)')
plt.ylabel('coupling coefficient')
plt.show()
##################################################################################
