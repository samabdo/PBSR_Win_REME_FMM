#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays

#========================================================= Define waveguide parameters =============================================================================
set_num_slab_modes(50)
reme.rememode.set_samplings(2001, 2001)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_ln = reme.AnisotropicMaterial(nx=2.2111, ny=2.1376, nz=2.2, name="LNOI")
m_sio2 = Material(1.444, 'm_sio2')                #value are from http://refractiveindex.info/
m_sin = Material(1.9963, 'm_sin')
m_air = Material(1, 'm_air')

# Dimensions
t_substrate = 1e-6       # thickness of the SiO2 substrate
t_core = 300e-9          # thickness of the SiN layer
t_ln = 300e-9            # thickness of the LN layer
t_cladding = 1e-6        # thickness of the top cladding
w_core = 2e-6


# Define parameters: waveguide width, gap, ring radius
#w00 = 0.2e-6
#w0 = 2e-6
w3 = 2.7e-6
w33 = 2.4e-6
w44 = 1.9e-6
w4 = 1.5e-6
wgp = 0.2e-6

wclad = 3e-6

# Taper length
ltp1 = 100e-6   #8 times longer than one used in silicon
ltp2 = 1000e-6
ltp3 = 150e-6
#ltp3 = ltp1*(w3-w2)/(w1-w0)
lt = ltp1 + ltp2 + ltp3


s_core = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sin(t_core) + m_air(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_air(t_core + t_cladding))


                     
#wg = reme.RWG(s_clad(wclad) + s_core(w00) + s_clad(wgp) + s_core(w3) + s_clad(wclad))



#wg.view()
dz = 5e-6
#length = 500e-6
#steps1 = int (round((lt/dz)/3))
steps = 33
dz1 = ltp1/steps;
dz2 = ltp2/steps;
dz3 = ltp3/steps;

dw1 = (w3-w33)/steps;
dw2 = (w33-w44)/steps;
dw3 = (w44-w4)/steps;

ws = np.linspace(w3, w33, steps)
wm = np.linspace(w33 - dw2, w44, steps)
we = np.linspace(w44 - dw3, w4, steps)
#steps = steps1 + steps2 + steps3

zs = np.linspace(0, ltp1, steps)
zm = np.linspace(ltp1 + dz2, ltp1+ltp2, steps)
ze = np.linspace(ltp1+ltp2 + dz3, lt, steps)

w = np.hstack((ws,wm,we))
z = np.hstack((zs,zm,ze))


plt.plot(z*1e6, w*1e6)
plt.xlabel('z (um)')
plt.ylabel('width (um)')
plt.show()


#z1 = np.linspace(0,length, steps)
#ww1 = np.linspace(w3, w4, steps)
#dif1 = 0
#dif2 = 0
#ww = 0

dz1 = ltp1/steps;
dz2 = ltp2/steps;
dz3 = ltp3/steps; 


neff_0 = np.zeros(len(z))
neff_1 = np.zeros(len(z))
neff_2 = np.zeros(len(z))

c01_array = []
c02_array = []
c12_array = []

c01_array = np.zeros(len(z))
c02_array = np.zeros(len(z))
c12_array = np.zeros(len(z))

#coupling_file = file('coupling_coefficient_dz_{}'.format(dz*1e6), 'w')



# View the waveguide
#wg.view()
#
for l in range (1, len(z) - 1, 1):
    print ("#####################################", l, "#########################################")
    print ("################################ z = ", z[l]*1e6, "######################################")
    
    
    wg = reme.RWG(s_clad(wclad) + s_core(w[l]) + s_clad(wclad))
    guide = reme.FEStraight(wg, 0.05e-6)

    #################################################get effective index (neff)##################
    guide.find_modes(3)
    
    o = guide.get_mode_effective_index(0).real
    p = guide.get_mode_effective_index(1).real
    q = guide.get_mode_effective_index(2).real
    
    print (o)
    print (p)
    print (q)
    
    neff_0[l] = o
    neff_1[l] = p
    neff_2[l] = q
    
#    guide.view_modes()
    #################################################get (beta) of th modes of interests#####
    k0 = 2.0*np.pi/wavelength
    omega = 2.0*np.pi*2.998e8/wavelength
    beta0 = k0*guide.get_mode_effective_index(0).real
    beta1 = k0*guide.get_mode_effective_index(1).real
    beta2 = k0*guide.get_mode_effective_index(2).real
    ################################################get refractive index (n) ###########################
    nxx = guide.get_nx(0)
    nyy = guide.get_ny(0)
    ################################################get fields (e)#############################
    e0 = np.zeros((nxx, nyy), complex)
    e1 = np.zeros((nxx, nyy), complex)
    e2 = np.zeros((nxx, nyy), complex)
    
    n0 = np.zeros((nxx, nyy))
    n1 = np.zeros((nxx, nyy))
    n2 = np.zeros((nxx, nyy))
    x = np.zeros(nxx)
    y = np.zeros(nyy)
    
    for i in range(0, nxx):
        x[i] = guide.get_x(0, i+1)
    for i in range(0, nyy):
        y[i] = guide.get_y(0, i+1)
    
    
    for i in range(0, nxx):
        for j in range (0, nyy):
            e0[i][j] = guide.get_E(0, i+1, j+1).x
            e1[i][j] = guide.get_E(1, i+1, j+1).x
            e2[i][j] = guide.get_E(2, i+1, j+1).x
    
            x0 = x[i]
            y0 = y[j]
            n1[i][j] = guide.get_refractive_index(x0, y0).real
            
    print("Calculating dn2/dz")
    #############################Define two waveguides at positions z-dz/2 and z+dz/2 to calculate dn^2/dz########
    
#    www = get_w(z[l]-dz, z[l], w[l], 0)
    # Define a waveguide with height, width and background material
    print (w[l-1])
    print (w[l])
    print (w[l+1])
    wg_0 = reme.RWG(s_clad(wclad) + s_core(w[l-1]) + s_clad(wclad))
    
    
    for i in range(0, nxx):
        for j in range (0, nyy):
            x0 = x[i]
            y0 = y[j]
            n0[i][j] = wg_0.get_refractive_index(x0, y0).real
    
#    www = get_w(z[l]+dz, z[l], w[l], 1)
    # Define a waveguide with height, width and background material
    wg_2 = reme.RWG(s_clad(wclad) + s_core(w[l+1]) + s_clad(wclad))
    
    
    for i in range(0, nxx):
        for j in range (0, nyy):
            x0 = x[i]
            y0 = y[j]
            n2[i][j] = wg_2.get_refractive_index(x0, y0).real
    ###############################################Calculating dn2_dz2##############################
    dn2_dz = np.zeros((len(x), len(y)), float)
#    out_file = file('dn2_dz2_{}.dat'.format(z[l]*1e6), 'w')
    for i in range(len(x)):
        for j in range(len(y)):
           dn2_dz[i][j] = (n2[i][j]**2 - n0[i][j]**2)/(dz)

    
    dx = (x[len(x)-1] - x[0])/(len(x)-1)
    dy = (y[len(y)-1] - y[0])/(len(y)-1)
    
    epsilon0 =  8.854187817e-12
    mu0 = 4.0e-7*np.pi
    print("calculating coupling coefficient")
    #########################################calculate coupling coefficienct ###############################    
    c01 = 0.0
    c02 = 0.0
    c12 = 0.0
    
    for i in range(len(x)):
        for j in range(len(y)):
            c01 += np.conjugate(e0[i][j])*e1[i][j]*dn2_dz[i][j]
            c02 += np.conjugate(e0[i][j])*e2[i][j]*dn2_dz[i][j]
            c12 += np.conjugate(e1[i][j])*e2[i][j]*dn2_dz[i][j]
        
    c01 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta1)*c01*(dx*dy)
    c02 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta2)*c02*(dx*dy)
    c12 = (epsilon0/mu0)**0.5*k0/4.0/(beta1-beta2)*c12*(dx*dy)
    
    print (c01)
    print (c02)
    print (c12)
    
    
    c01_array[l] = c01
    c02_array[l] = c02
    c12_array[l] = c12
    
#    coupling_file.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(z[l]*1e6, beta0, beta1, beta2, np.abs(c01), np.abs(c02), np.abs(c12)))
#
#coupling_file.close()
    
print("Done")

neff_0[0]=neff_0[1] ##making the size of neff the same size as z by equalizing first 2 and last 2 terms
neff_1[0]=neff_1[1]
neff_2[0]=neff_2[1]

neff_0[len(z)-1]=neff_0[len(z)-2] ##making the size of neff the same size as z by equalizing first 2 and last 2 terms
neff_1[len(z)-1]=neff_1[len(z)-2]
neff_2[len(z)-1]=neff_2[len(z)-2]

c01_array[0]=c01_array[1]
c02_array[0]=c02_array[1]
c12_array[0]=c12_array[1]

c01_array[len(z)-1]= c01_array[len(z)-2]
c02_array[len(z)-1]= c02_array[len(z)-2]
c12_array[len(z)-1]= c12_array[len(z)-2]

plt.plot(z*1e6, neff_0, 'r', z*1e6, neff_1, 'b', z*1e6, neff_2, 'g')
plt.xlabel('z (um)')
plt.ylabel('neff')

#plt.legend(handles=[line_1, line_2, line_3])
plt.show()

plt.plot(z*1e6, c01_array, 'r', z*1e6, c02_array, 'b', z*1e6, c12_array, 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.show()
    
plt.plot(z*1e6, abs(c01_array), 'r', z*1e6, abs(c02_array), 'b', z*1e6, abs(c12_array), 'g')
plt.xlabel('z (um)')
plt.ylabel('coupling coefficient')
plt.show()

