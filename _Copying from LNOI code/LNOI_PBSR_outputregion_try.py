#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.nan) ##print full arrays

#========================================================= Define waveguide parameters =============================================================================
set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_ln = reme.AnisotropicMaterial(nx=2.2111, ny=2.1376, nz=2.2, name="LNOI")
m_sio2 = Material(1.444, 'SiO2')                #value are from http://refractiveindex.info/
m_sin = Material(1.9963, 'SiN')
m_air = Material(1, 'Air')

# Dimensions
t_substrate = 1e-6       # thickness of the SiO2 substrate
t_core = 300e-9          # thickness of the SiN layer
t_ln = 300e-9            # thickness of the LN layer
t_cladding = 1e-6        # thickness of the top cladding
w_core = 2e-6


##### 1 - Define parameters: taper widths, gap and total width
w00 = 2e-6
w0 = 1e-6
w3 = 3e-6
w4 = 1e-6
wgp1 = 0.2e-6
wgp2 = 2e-6
wtot = 12e-6

dz = 20e-6
length = 1000e-6
steps = int (round(length/dz))
z = np.linspace(0, length, steps)
#w = np.linspace(w3, w4, steps)

wcn = np.linspace(w00, w0, steps)
wcw = np.linspace(w3, w4, steps)
wgp = np.linspace (wgp1, wgp2, steps)

########################################################

##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
ycentre = wtot/2  ###ridge centre y location
xsin = t_substrate + t_ln + t_core/2  ###ridge centre x location of wg

yr1 = ycentre + 0.5*wgp[20]
yl1 = ycentre - 0.5*wgp[20]
yr0 = ycentre + 0.5*wgp[19]
yl0 = ycentre - 0.5*wgp[19]
yr2 = ycentre + 0.5*wgp[21]
yl2 = ycentre - 0.5*wgp[21]

yleft1 = yl1 - wcn[20]
yright1 = yr1 + wcw[20]
yleft0 = yl0 - wcn[19]
yright0 = yr0 + wcw[19]
yleft2 = yl2 - wcn[21]
yright2 = yr2 + wcw[21]

wsl = yleft2 - yleft0 ###width of left stripe
wsr = yright0 - yright2 ###width of right stripe
wsml = yl0 - yl2
wsmr = yr2 - yr0
stripe_t = t_core ###Stripes height
print 'left stripe width =', wsl
print 'right stripe width =', wsr
print 'middle left stripe width =', wsml
print 'middle right stripe width =', wsmr
#############################################################################


##### 3 - Define polygons, complex waveguide and adding areas of different meshings
p_sub = Polygon([(0, 0), (0, wtot), (t_substrate, wtot), (t_substrate, 0)])
p_ln = Polygon([(t_substrate, 0), (t_substrate, wtot), (t_substrate+t_ln, wtot), (t_substrate+t_ln, 0)])
p_sincn = Polygon([(t_substrate+t_ln, yleft2), (t_substrate+t_ln, yl2), (t_substrate+t_ln+t_core, yl2), (t_substrate+t_ln+t_core, yleft2)]) ####centre SiN ridge width minus half strip width, with regular meshing
p_sincw = Polygon([(t_substrate+t_ln, yr2), (t_substrate+t_ln, yright2), (t_substrate+t_ln+t_core, yright2), (t_substrate+t_ln+t_core, yr2)])

p_sinml = Polygon([(t_substrate+t_ln, yl2), (t_substrate+t_ln, yl1), (t_substrate+t_ln+t_core, yl1), (t_substrate+t_ln+t_core, yl2)]) ####left SiN ridge width minus half strip width, with fine meshing
p_sinmr = Polygon([(t_substrate+t_ln, yr1), (t_substrate+t_ln, yr2), (t_substrate+t_ln+t_core,yr2), (t_substrate+t_ln+t_core, yr1)]) ####right SiN ridge width minus half strip width, with fine meshing
p_airml = Polygon([(t_substrate+t_ln, yl1), (t_substrate+t_ln, yl0), (t_substrate+t_ln+t_core, yl0), (t_substrate+t_ln+t_core, yl1)]) ####left Air rectangle with the the other half of strip width, with fine meshing
p_airmr = Polygon([(t_substrate+t_ln, yr0), (t_substrate+t_ln, yr1), (t_substrate+t_ln+t_core, yr1), (t_substrate+t_ln+t_core, yr0)]) ####right Air rectangle with the the other half of strip width, with fine meshing


p_sinl = Polygon([(t_substrate+t_ln, yleft1), (t_substrate+t_ln, yleft2), (t_substrate+t_ln+t_core, yleft2), (t_substrate+t_ln+t_core, yleft1)]) ####left SiN ridge width minus half strip width, with fine meshing
p_sinr = Polygon([(t_substrate+t_ln, yright2), (t_substrate+t_ln, yright1), (t_substrate+t_ln+t_core, yright1), (t_substrate+t_ln+t_core, yright2)]) ####right SiN ridge width minus half strip width, with fine meshing
p_airl = Polygon([(t_substrate+t_ln, yleft0), (t_substrate+t_ln, yleft1), (t_substrate+t_ln+t_core, yleft1), (t_substrate+t_ln+t_core, yleft0)]) ####left Air rectangle with the the other half of strip width, with fine meshing
p_airr = Polygon([(t_substrate+t_ln, yright1), (t_substrate+t_ln, yright0), (t_substrate+t_ln+t_core, yright0), (t_substrate+t_ln+t_core, yright1)]) ####right Air rectangle with the the other half of strip width, with fine meshing

wg = ComplexWaveguide(t_substrate + t_ln + t_core + t_cladding, wtot, m_air) ###Define a complex waveguide with height, width and background material

wg.add_area(p_sub, m_sio2)  ###Adding substrate layer, with regular meshing
wg.add_area(p_ln, m_ln)  ###Adding LN layer, with regular meshing
wg.add_area(p_sincn, m_sin)  ###Adding SiN layer, with regular meshing
wg.add_area(p_sincw, m_sin)  ###Adding SiN layer, with regular meshing
wg.add_area(p_sinml, m_sin, 0.001e-6)  ###Adding left SiN layer, with fine meshing
wg.add_area(p_sinmr, m_sin, 0.001e-6)  ###Adding right SiN layer, with fine meshing
wg.add_area(p_airml, m_air, 0.001e-6)  ###Adding left air layer, with fine meshing
wg.add_area(p_airmr, m_air, 0.001e-6)  ###Adding right air layer, with fine meshing
wg.add_area(p_sinl, m_sin, 0.001e-6)  ###Adding left SiN layer, with fine meshing
wg.add_area(p_sinr, m_sin, 0.001e-6)  ###Adding right SiN layer, with fine meshing
wg.add_area(p_airl, m_air, 0.001e-6)  ###Adding left air layer, with fine meshing
wg.add_area(p_airr, m_air, 0.001e-6)  ###Adding right air layer, with fine meshing

#wg.view(aspect_ratio_equal=False) ###View the waveguide structure
#guide = reme.FEStraight(wg, 0.05e-6)
#guide.view()
#guide.view_mesh()
########################################################################################

####### 4 - Defining wg_0 and wg_2
s_core = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sin(t_core) + m_air(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_air(t_core + t_cladding))


wg_0 = reme.RWG(s_clad(yleft0) + s_core(wcn[19]) + s_clad(wgp[19]) + s_core(wcw[19]) + s_clad(wtot - yright0))
wg_2 = reme.RWG(s_clad(yleft2) + s_core(wcn[21]) + s_clad(wgp[21]) + s_core(wcw[21]) + s_clad(wtot - yright2))

#####################################

####### 5 - Getting n0 and n2
n0l = wg_0.get_refractive_index(xsin, yleft1).real
print 'n0l =',n0l    
n2l = wg_2.get_refractive_index(xsin, yleft1).real
print 'n2l =',n2l


#################################

####### 6 - Calculating dn2/dz and constructing an array for dn2/dz
dn = n2l - n0l
print 'dnl =', dn  
dn2_dz = (n2l**2 - n0l**2)/(2*dz)
print 'dn2/dz =', dn2_dz
nxx = 100
nyy = 100
dn2_dz_array = dn2_dz*np.ones((nxx,nyy))


####!!!!!!!!!!!!!!!!because the gradient in the width is exactly the same on left and right, one can just reverse the sign of dn2/dz



#dnr = n2r - n0r
#print 'dnr =', dnr  
#dn2_dzr = (n2r**2 - n0r**2)/(2*dz)
#print 'dn2/dzr =', dn2_dzr
#nxx = 100
#nyy = 100
#dn2_dz_arrayr = dn2_dzr*np.ones((nxx,nyy))
####################################################################
#
######### 7 - Using FEM solver and finding modes
guide = reme.FEStraight(wg, 0.05e-6)
#guide.view()
guide.find_modes(3)
#guide.view_mesh()
#guide.view_modes()
################################################

######## 8 - Calculating betas and defining constants
k0 = 2.0*np.pi/wavelength
epsilon0 =  8.854187817e-12
mu0 = 4.0e-7*np.pi
beta0 = k0*guide.get_mode_effective_index(0).real
beta1 = k0*guide.get_mode_effective_index(1).real
beta2 = k0*guide.get_mode_effective_index(2).real
dx = stripe_t/(nxx-1)
dy = wsl/(nyy-1)
################################################

####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
xleft = xright = np.linspace(t_substrate + t_ln, t_substrate + t_ln + t_core, nxx)
xmleft = xmright = np.linspace(t_substrate + t_ln, t_substrate + t_ln + t_core, nxx)
yleft = np.linspace(yleft2, yleft0, nyy)
yright = np.linspace(yright2, yright0, nyy)

ymleft = np.linspace(yl2, yl0, nyy)
ymright = np.linspace(yr0, yr2, nyy)

e0_leftx = np.zeros((nxx, nyy), complex)
e1_leftx = np.zeros((nxx, nyy), complex)
e2_leftx = np.zeros((nxx, nyy), complex)
e0_lefty = np.zeros((nxx, nyy), complex)
e1_lefty = np.zeros((nxx, nyy), complex)
e2_lefty = np.zeros((nxx, nyy), complex)
e0_leftz = np.zeros((nxx, nyy), complex)
e1_leftz = np.zeros((nxx, nyy), complex)
e2_leftz = np.zeros((nxx, nyy), complex)
e0_rightx = np.zeros((nxx, nyy), complex)
e1_rightx = np.zeros((nxx, nyy), complex)
e2_rightx = np.zeros((nxx, nyy), complex)
e0_righty = np.zeros((nxx, nyy), complex)
e1_righty = np.zeros((nxx, nyy), complex)
e2_righty = np.zeros((nxx, nyy), complex)
e0_rightz = np.zeros((nxx, nyy), complex)
e1_rightz = np.zeros((nxx, nyy), complex)
e2_rightz = np.zeros((nxx, nyy), complex)

e0_mleftx = np.zeros((nxx, nyy), complex)
e1_mleftx = np.zeros((nxx, nyy), complex)
e2_mleftx = np.zeros((nxx, nyy), complex)
e0_mlefty = np.zeros((nxx, nyy), complex)
e1_mlefty = np.zeros((nxx, nyy), complex)
e2_mlefty = np.zeros((nxx, nyy), complex)
e0_mleftz = np.zeros((nxx, nyy), complex)
e1_mleftz = np.zeros((nxx, nyy), complex)
e2_mleftz = np.zeros((nxx, nyy), complex)
e0_mrightx = np.zeros((nxx, nyy), complex)
e1_mrightx = np.zeros((nxx, nyy), complex)
e2_mrightx = np.zeros((nxx, nyy), complex)
e0_mrighty = np.zeros((nxx, nyy), complex)
e1_mrighty = np.zeros((nxx, nyy), complex)
e2_mrighty = np.zeros((nxx, nyy), complex)
e0_mrightz = np.zeros((nxx, nyy), complex)
e1_mrightz = np.zeros((nxx, nyy), complex)
e2_mrightz = np.zeros((nxx, nyy), complex)

for i in range(0, nxx):
        for j in range (0, nyy):
            ####field at left stripe
            e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
            e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
            e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
            e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
            e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
            e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
            e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
            e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
            e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
            ####field at right stripe
            e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
            e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
            e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
            e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
            e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
            e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
            e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
            e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
            e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z
            
            ####field at middle left stripe
            e0_mleftx[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).x
            e1_mleftx[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).x
            e2_mleftx[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).x
            e0_mlefty[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).y
            e1_mlefty[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).y
            e2_mlefty[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).y
            e0_mleftz[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).z
            e1_mleftz[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).z
            e2_mleftz[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).z
            ####field at middle right stripe
            e0_mrightx[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).x
            e1_mrightx[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).x
            e2_mrightx[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).x
            e0_mrighty[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).y
            e1_mrighty[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).y
            e2_mrighty[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).y
            e0_mrightz[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).z
            e1_mrightz[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).z
            e2_mrightz[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).z
##############################################################################################            
            
####### 10 - Calculating coupling coefficient
c01l = 0.0
c02l = 0.0
c12l = 0.0
c01r = 0.0
c02r = 0.0
c12r = 0.0

c01ml = 0.0
c02ml = 0.0
c12ml = 0.0
c01mr = 0.0
c02mr = 0.0
c12mr = 0.0

c01 = 0.0
c02 = 0.0
c12 = 0.0

for i in range(nxx): ###Dot product of the fields at the left and right stripes
        for j in range(nyy):
            c01l += np.conjugate(e0_leftx[i][j])*e1_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j])*e1_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j])*e1_leftz[i][j]*dn2_dz_array[i][j]
            c02l += np.conjugate(e0_leftx[i][j])*e2_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j])*e2_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j])*e2_leftz[i][j]*dn2_dz_array[i][j]
            c12l += np.conjugate(e1_leftx[i][j])*e2_leftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_lefty[i][j])*e2_lefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_leftz[i][j])*e2_leftz[i][j]*dn2_dz_array[i][j]
            c01r += np.conjugate(e0_rightx[i][j])*e1_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j])*e1_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j])*e1_rightz[i][j]*dn2_dz_array[i][j]
            c02r += np.conjugate(e0_rightx[i][j])*e2_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j])*e2_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j])*e2_rightz[i][j]*dn2_dz_array[i][j]
            c12r += np.conjugate(e1_rightx[i][j])*e2_rightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_righty[i][j])*e2_righty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_rightz[i][j])*e2_rightz[i][j]*dn2_dz_array[i][j]
            
            c01ml += np.conjugate(e0_mleftx[i][j])*e1_mleftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mlefty[i][j])*e1_mlefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mleftz[i][j])*e1_mleftz[i][j]*dn2_dz_array[i][j]
            c02ml += np.conjugate(e0_mleftx[i][j])*e2_mleftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mlefty[i][j])*e2_mlefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mleftz[i][j])*e2_mleftz[i][j]*dn2_dz_array[i][j]
            c12ml += np.conjugate(e1_mleftx[i][j])*e2_mleftx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_mlefty[i][j])*e2_mlefty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_mleftz[i][j])*e2_mleftz[i][j]*dn2_dz_array[i][j]
            c01mr += np.conjugate(e0_mrightx[i][j])*e1_mrightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mrighty[i][j])*e1_mrighty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mrightz[i][j])*e1_mrightz[i][j]*dn2_dz_array[i][j]
            c02mr += np.conjugate(e0_mrightx[i][j])*e2_mrightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mrighty[i][j])*e2_mrighty[i][j]*dn2_dz_array[i][j] + np.conjugate(e0_mrightz[i][j])*e2_mrightz[i][j]*dn2_dz_array[i][j]
            c12mr += np.conjugate(e1_mrightx[i][j])*e2_mrightx[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_mrighty[i][j])*e2_mrighty[i][j]*dn2_dz_array[i][j] + np.conjugate(e1_mrightz[i][j])*e2_mrightz[i][j]*dn2_dz_array[i][j]


c01 = c01l + c01r + c01ml + c01mr
c02 = c02l + c02r + c02ml + c02mr
c12 = c12l + c12r + c12ml + c12mr

c01 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta1)*c01*(dx*dy)
c02 = (epsilon0/mu0)**0.5*k0/4.0/(beta0-beta2)*c02*(dx*dy)
c12 = (epsilon0/mu0)**0.5*k0/4.0/(beta1-beta2)*c12*(dx*dy)

print (c01)
print (c02)
print (c12)
