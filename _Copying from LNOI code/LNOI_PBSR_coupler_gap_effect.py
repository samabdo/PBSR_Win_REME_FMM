#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
import pickle


#========================================================= importing parameters =============================================================================
#####dbeta vs wcn
with open('LNOI_PBSR_adiabatic_coupler_dbeta_g200', 'rb') as f:
    dbetag200 = pickle.load(f)
    
with open('LNOI_PBSR_adiabatic_coupler_dbeta_g400', 'rb') as f:
    dbetag400 = pickle.load(f)
    
with open('LNOI_PBSR_adiabatic_coupler_dbeta_g600', 'rb') as f:
    dbetag600 = pickle.load(f)
    
with open('LNOI_PBSR_adiabatic_coupler_dbeta_g800', 'rb') as f:
    dbetag800 = pickle.load(f)
    
with open('LNOI_PBSR_adiabatic_coupler_wcn_g200', 'rb') as f:
    wcng200 = pickle.load(f)
    
###wcn  vs z
with open('LNOI_PBSR_adiabatic_coupler_compact_z', 'rb') as f:
    z200 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_wn', 'rb') as f:
    wn200 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_ww', 'rb') as f:
    ww200 = pickle.load(f)
    
with open('LNOI_PBSR_adiabatic_coupler_compact_g400_z', 'rb') as f:
    z400 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_g400_wn', 'rb') as f:
    wn400 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_g400_ww', 'rb') as f:
    ww400 = pickle.load(f)

with open('LNOI_PBSR_adiabatic_coupler_compact_g600_z', 'rb') as f:
    z600 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_g600_wn', 'rb') as f:
    wn600 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_g600_ww', 'rb') as f:
    ww600 = pickle.load(f) 
   
with open('LNOI_PBSR_adiabatic_coupler_compact_g800_z', 'rb') as f:
    z800 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_g800_wn', 'rb') as f:
    wn800 = pickle.load(f)
with open('LNOI_PBSR_adiabatic_coupler_compact_g800_ww', 'rb') as f:
    ww800 = pickle.load(f)
#########plotting            
plt.plot(wcng200*1e6, dbetag200, label='g = 200 nm')
plt.plot(wcng200*1e6, dbetag400, label='g = 400 nm')
plt.plot(wcng200*1e6, dbetag600, label='g = 600 nm')
plt.plot(wcng200*1e6, dbetag800, label='g = 800 nm')
plt.xlabel('Narrow Waveguide Width (um)')
plt.ylabel('Delta Beta')
plt.title("Gap Effect on delta beta")
plt.legend(loc = 'upper left')
plt.show()


#########plotting            
plt.plot(z200, wn200, label='g = 200 nm')
plt.plot(z400, wn400, label='g = 400 nm')
plt.plot(z600, wn600, label='g = 600 nm')
plt.plot(z800, wn800, label='g = 800 nm')
plt.xlabel('z (um)')
plt.ylabel('Narrow Waveguide Width (um)')
plt.title("Gap Effect on Narrow Waveguide Width")
plt.legend()
plt.show()


#########plotting            
plt.plot(z200, ww200, label='g = 200 nm')
plt.plot(z400, ww400, label='g = 400 nm')
plt.plot(z600, ww600, label='g = 600 nm')
plt.plot(z800, ww800, label='g = 800 nm')
plt.xlabel('z (um)')
plt.ylabel('Wide Waveguide Width (um)')
plt.title("Gap Effect on Wide Waveguide Width")
plt.legend()
plt.show()