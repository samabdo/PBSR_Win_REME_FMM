#PhD Project: Integrated Nonlinear Photonics in Silicon Nitride on Lithium Niobate Platform
"""
Created on Wed Nov 29 11:22:34 2017

@author: s3575131
"""

#######################################################################################################################################################################
#================================================================ Objective ===========================================================================================

# Simulate a polarization beam splitter based combining an adiabatic taper and an asymmetrical directional coupler.

# Refractive indices values are from http://refractiveindex.info/

#######################################################################################################################################################################
#=============================================================== WG dimensions=========================================================================================

# Waveguide original idea is from paper Thin film wavelength converters for photonic integrated circuits by Chang et al
# PBS idea from paper Novel concept for ultracompact polarization splitter-rotator based on silicon nanowires by Dai et al. 2011

# Waveguide structure from Bottom to top: LN (NA, slab), SiO2 (2 um, slab), LN (0.7 um, slab), SiN (0.39 um, 2 um rib), SiO2 (1 um, upper cladding)
# Note: in the paper, the fabricated SiN thickness was 0.35 um forming a ridge with the remaining 0.04 is left unetched.


#                                                                        ______________
#                                                          _____________|______SiN____|_______________
###########################################################____________________LN______________________############################################################
#                                                                             SiO2
###########################################################____________________________________________############################################################
##########################################################|
##########################################################|                   LN
##########################################################|____________________________________________############################################################ 

#######################################################################################################################################################################


#============================================================== Import Libraries ==================================================================================
from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt


#========================================================= Define waveguide parameters =============================================================================


# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)
k0 = 2.0*np.pi/wavelength

nm = 5 #### number of modes to find

m_ln = reme.AnisotropicMaterial(nx=2.2111, ny=2.1376, nz=2.2, name="LNOI")
m_sio2 = Material(1.444, 'm_sio2')                #value are from http://refractiveindex.info/
m_sin = Material(1.9963, 'm_sin')
m_air = Material(1, 'm_air')

# Dimensions
t_substrate = 1e-6       # thickness of the SiO2 substrate
t_core = 300e-9          # thickness of the SiN layer
t_ln = 300e-9            # thickness of the LN layer
t_cladding = 1e-6        # thickness of the top cladding
#w_core = 2e-6


# Define parameters: waveguide width, gap, ring radius
w00 = 2e-6
w0 = 1e-6
w3 = 3e-6
w4 = 1e-6
wgp1 = 0.2e-6
wgp2 = 2.5e-6

#wtot = 8e-6

wclad = 3e-6


set_num_slab_modes(50)
#set_fmm_scan_step(1e-4)

# Define core and cladding slabs
#s_core = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sin(t_core) + m_sio2(t_cladding))
#s_clad = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sio2(t_core + t_cladding))


s_core = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_sin(t_core) + m_air(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_ln(t_ln) + m_air(t_core + t_cladding))


                     
rwg_wire = reme.RWG(s_clad(wclad) + s_core(w00) + s_clad(wgp1) + s_core(w3) + s_clad(wclad))

rwg_wire.view()

#============================================================= Simulation ========================================================================================

straight_wire = reme.FEStraight(rwg_wire, 0.05e-6)
# Enclose the waveguide within PEC boundaries
#straight_wire.set_left_boundary(reme.PMC)
   
num_points = 61
wcn = np.linspace(w00, w0, num_points)
wcw = np.linspace(w3, w4, num_points)
wgp = np.linspace (wgp1, wgp2, num_points)
num = np.linspace(0, num_points, num_points)
waveguide_neff = np.zeros((100, num_points))
te_fraction = np.zeros((100, num_points))


for i in range(num_points):
    straight_wire = reme.FEStraight(rwg_wire, 0.05e-6)
    # Enclose the waveguide within PEC boundaries
#    straight_wire.set_left_boundary(reme.PMC)
    rwg_wire.set_width(2, wcn[i])
    rwg_wire.set_width(3, wgp[i])
    rwg_wire.set_width(4, wcw[i])
#    straight_wire.polish_mode_list()
    straight_wire.find_modes(nm, 2.211, 1.5, scan_step=0.05e-6)
    print "Here"
    for k in range(nm):
        n = straight_wire.get_mode_effective_index(k).real
        if n > 1.5:
            waveguide_neff[k][i] = n
            te_fraction[k][i] = straight_wire.get_TE_fraction(k)
 

betas=np.zeros((100, num_points))
betas = k0*waveguide_neff
dbeta1 = betas[0] - betas[1]   
dbeta2 = betas[1] - betas[2]          
#########plotting            
f, ax = plt.subplots()
for k in range(nm):
    ax.plot(num, waveguide_neff[k])
    
ax.set(xlabel='Waveguide Width', ylabel='Effective Index', title='Output Region Super Modes')
#ax.grid()
ax.set_xlim([0, num_points-1])
ax.set_ylim(1.82, 1.9)
plt.show()

plt.plot(num, dbeta1, label = 'TE1 - TE0')
plt.plot(num, dbeta2, label = 'TE1 - TE2')
plt.xlabel('Waveguide Width (um)')
plt.ylabel('Delta Beta')
#ax.grid()
#plt.set_xlim([0, num_points-1])
plt.legend(loc = 'upper left')
plt.show()


##################Looking at the modes
#####start
rwg = reme.RWG(s_clad(wclad) + s_core(wcn[0]) + s_clad(wgp[0]) + s_core(wcw[0]) + s_clad(wclad))
rwg.view()
guide = reme.FEStraight(rwg, 0.05e-6)
guide.find_modes(3, 2.211, 1.5)
guide.view_modes()
#####middle
rwg = reme.RWG(s_clad(wclad) + s_core(wcn[(len(wcn)-1)/2]) + s_clad(wgp[(len(wcn)-1)/2]) + s_core(wcw[(len(wcn)-1)/2]) + s_clad(wclad))
rwg.view()
guide = reme.FEStraight(rwg, 0.05e-6)
guide.find_modes(3, 2.211, 1.5)
guide.view_modes()
#####end
rwg = reme.RWG(s_clad(wclad) + s_core(wcn[len(wcn)-1]) + s_clad(wgp[len(wcw)-1]) + s_core(wcw[len(wcw)-1]) + s_clad(wclad))
rwg.view()
guide = reme.FEStraight(rwg, 0.05e-6)
guide.find_modes(3, 2.211, 1.5)
guide.view_modes()