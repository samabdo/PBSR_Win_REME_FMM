# -*- coding: utf-8 -*-
"""
Created on Thurs Oct 05 11:22:34 2017

@author: s3575131
"""

from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt

wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6       # thickness of the SiO2 substrate
t_core = 220e-9          # thickness of the core
t_cladding = 1e-6        # thickness of the top cladding
w_core = 1e-6
w_clad = 1e-6

s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))



rwg_wire = reme.RWG(s_clad(w_clad) +
                     s_core(w_core) + 
                     s_clad(w_clad))
rwg_wire.view()

straight_wire = reme.FMMStraight(rwg_wire)

# Enclose the waveguide within PEC boundaries
straight_wire.set_left_boundary(reme.PEC)
straight_wire.set_right_boundary(reme.PEC)
straight_wire.set_top_boundary(reme.PEC)
straight_wire.set_bottom_boundary(reme.PEC)

straight_wire.find_mode(2.3)
straight_wire.plot_mode(mode_number=0, field_component='Ey')

rwg_wire.set_width(2, 1e-6) # make the width of the second section (the core section) equal to 1um

# Find maximum 10 modes with effective indices within Si and SiO2 refractive indices
straight_wire.find_modes(10, m_si.n().real, m_sio2.n().real)

num_modes = straight_wire.number_waveguide_modes()
print "Found", num_modes, "modes"

straight_wire.plot_mode(mode_number=0, field_component='Ey')
straight_wire.plot_mode(mode_number=1, field_component='Ey')

for i in range(num_modes):
    print("Mode {}: neff = {}, TE fraction = {}".
          format(i, straight_wire.get_mode_effective_index(i).real, straight_wire.get_TE_fraction(i)))
    
num_points = 61
widths = np.linspace(1.5e-6, 0.3e-6, num_points)
waveguide_neff = np.zeros((10, num_points))
te_fraction = np.zeros((10, num_points))

for i in range(num_points):
    rwg_wire.set_width(2, widths[i])
    straight_wire.polish_mode_list()
    straight_wire.find_modes(10, m_si.n().real, m_sio2.n().real, clear_existing_modes=True, scan_step=1e-3)
    for k in range(10):
        n = straight_wire.get_mode_effective_index(k).real
        if n > m_sio2.n().real:
            waveguide_neff[k][i] = n
            te_fraction[k][i] = straight_wire.get_TE_fraction(k)
            
            


k0 = 2.0*np.pi/wavelength
betas = np.zeros((10, num_points))
betas = k0 * waveguide_neff
dbeta = betas[1] - betas[2]
#########plotting
f, ax = plt.subplots()
for k in range(10):
    ax.plot(widths * 1e6, waveguide_neff[k])

ax.set(xlabel='Waveguide Width (um)', ylabel='Effective Index', title='Taper Modes')
# ax.grid()
# ax.set_xlim([0, num_points-1])
# ax.set_ylim(1.7, 1.95)
plt.show()

plt.plot(widths * 1e6, dbeta)
plt.xlabel('Waveguide Width (um)')
plt.ylabel('Delta Beta')
# ax.grid()
plt.show()

