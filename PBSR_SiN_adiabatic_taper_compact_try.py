# -*- coding: utf-8 -*-
"""
Created on Thurs Oct 05 11:22:34 2017

@author: s3575131
"""

from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt

wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding
w_core = 1e-6



num_points = 101
widths = np.linspace(0.54e-6, 0.9e-6, num_points)
dz = 2e-6
length = 100e-6
z = np.linspace(0, length, num_points)
###################################################################

wtot = 5e-6
w_clad = (wtot - w_core)/2
ycentre = wtot / 2  ###ridge centre y location
xsin = t_substrate + t_core / 2  ###ridge centre x location of wg

yleft1 = ycentre - widths[55] / 2
yright1 = ycentre + widths[55] / 2
yleft2 = ycentre - widths[54] / 2
yright2 = ycentre + widths[54] / 2
yleft0 = ycentre - widths[56]/ 2
yright0 = ycentre + widths[56] / 2

wsl = yleft2 - yleft0  ###width of left stripe
wsr = yright0 - yright2  ###width of right stripe
stripe_t = t_core  ###Stripes height
print 'left stripe width =', wsl
print 'right stripe width =', wsr


########Finding modes for the main waveguide (wg1)
s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
rwg_wire = reme.RWG(s_clad(w_clad) + s_core(widths[55]) + s_clad(w_clad))
straight_wire = reme.FMMStraight(rwg_wire)
straight_wire.set_left_boundary(reme.PEC)
straight_wire.set_right_boundary(reme.PEC)
straight_wire.set_top_boundary(reme.PEC)
straight_wire.set_bottom_boundary(reme.PEC)

straight_wire.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
#############################################

########Defining wg_0 and wg_2 and getting their refractive indexes, and dn2_dz
rwg_wire0 = reme.RWG(s_clad(w_clad) + s_core(widths[54]) + s_clad(w_clad))
rwg_wire2 = reme.RWG(s_clad(w_clad) + s_core(widths[56]) + s_clad(w_clad))

n2 = 2.0
n0 = 3.455
dn  = n2 - n0
dn2_dz = (n2**2 - n0**2)/(2*dz)
nxx = 100
nyy = 100
dn2_dz_array = dn2_dz*np.ones((nxx,nyy))
###############################################################################

########Defining constants and calculating betas
k0 = 2.0 * np.pi / wavelength
epsilon0 =  8.854187817e-12
mu0 = 4.0e-7*np.pi
beta0 = k0*straight_wire.get_mode_effective_index(0).real
beta1 = k0*straight_wire.get_mode_effective_index(1).real
beta2 = k0*straight_wire.get_mode_effective_index(2).real
dx = stripe_t / (nxx - 1)
dy = wsl / (nyy - 1)
#########################################################
xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
yleft = np.linspace(yleft0, yleft2, nyy)
yright = np.linspace(yright2, yright0, nyy)

e0_leftx = np.zeros((nxx, nyy), complex)
e1_leftx = np.zeros((nxx, nyy), complex)
e2_leftx = np.zeros((nxx, nyy), complex)
e0_lefty = np.zeros((nxx, nyy), complex)
e1_lefty = np.zeros((nxx, nyy), complex)
e2_lefty = np.zeros((nxx, nyy), complex)
e0_leftz = np.zeros((nxx, nyy), complex)
e1_leftz = np.zeros((nxx, nyy), complex)
e2_leftz = np.zeros((nxx, nyy), complex)
e0_rightx = np.zeros((nxx, nyy), complex)
e1_rightx = np.zeros((nxx, nyy), complex)
e2_rightx = np.zeros((nxx, nyy), complex)
e0_righty = np.zeros((nxx, nyy), complex)
e1_righty = np.zeros((nxx, nyy), complex)
e2_righty = np.zeros((nxx, nyy), complex)
e0_rightz = np.zeros((nxx, nyy), complex)
e1_rightz = np.zeros((nxx, nyy), complex)
e2_rightz = np.zeros((nxx, nyy), complex)

for i in range(0, nxx):
    for j in range(0, nyy):
        ####field at left stripe
        e0_leftx[i][j] = straight_wire.get_E_field(0, xleft[i], yleft[j]).x
        e1_leftx[i][j] =straight_wire.get_E_field(1, xleft[i], yleft[j]).x
        e2_leftx[i][j] = straight_wire.get_E_field(2, xleft[i], yleft[j]).x
        e0_lefty[i][j] = straight_wire.get_E_field(0, xleft[i], yleft[j]).y
        e1_lefty[i][j] = straight_wire.get_E_field(1, xleft[i], yleft[j]).y
        e2_lefty[i][j] = straight_wire.get_E_field(2, xleft[i], yleft[j]).y
        e0_leftz[i][j] = straight_wire.get_E_field(0, xleft[i], yleft[j]).z
        e1_leftz[i][j] = straight_wire.get_E_field(1, xleft[i], yleft[j]).z
        e2_leftz[i][j] = straight_wire.get_E_field(2, xleft[i], yleft[j]).z
        ####field at right stripe
        e0_rightx[i][j] = straight_wire.get_E_field(0, xright[i], yright[j]).x
        e1_rightx[i][j] = straight_wire.get_E_field(1, xright[i], yright[j]).x
        e2_rightx[i][j] = straight_wire.get_E_field(2, xright[i], yright[j]).x
        e0_righty[i][j] = straight_wire.get_E_field(0, xright[i], yright[j]).y
        e1_righty[i][j] = straight_wire.get_E_field(1, xright[i], yright[j]).y
        e2_righty[i][j] = straight_wire.get_E_field(2, xright[i], yright[j]).y
        e0_rightz[i][j] = straight_wire.get_E_field(0, xright[i], yright[j]).z
        e1_rightz[i][j] = straight_wire.get_E_field(1, xright[i], yright[j]).z
        e2_rightz[i][j] = straight_wire.get_E_field(2, xright[i], yright[j]).z
##############################################################################################

####### 10 - Calculating coupling coefficient
c01l = 0.0
c02l = 0.0
c12l = 0.0
c01r = 0.0
c02r = 0.0
c12r = 0.0
c01 = 0.0
c02 = 0.0
c12 = 0.0

for i in range(nxx):  ###Dot product of the fields at the left and right stripes
    for j in range(nyy):
        c01l += np.conjugate(e0_leftx[i][j]) * e1_leftx[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j]) * \
                e1_lefty[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j]) * e1_leftz[i][j] * dn2_dz_array[i][j]
        c02l += np.conjugate(e0_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_lefty[i][j]) * \
                e2_lefty[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_leftz[i][j]) * e2_leftz[i][j] * dn2_dz_array[i][j]
        c12l += np.conjugate(e1_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_array[i][j] + np.conjugate(e1_lefty[i][j]) * \
                e2_lefty[i][j] * dn2_dz_array[i][j] + np.conjugate(e1_leftz[i][j]) * e2_leftz[i][j] * dn2_dz_array[i][j]
        c01r += np.conjugate(e0_rightx[i][j]) * e1_rightx[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j]) * \
                e1_righty[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j]) * e1_rightz[i][j] * \
                dn2_dz_array[i][j]
        c02r += np.conjugate(e0_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_righty[i][j]) * \
                e2_righty[i][j] * dn2_dz_array[i][j] + np.conjugate(e0_rightz[i][j]) * e2_rightz[i][j] * \
                dn2_dz_array[i][j]
        c12r += np.conjugate(e1_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_array[i][j] + np.conjugate(e1_righty[i][j]) * \
                e2_righty[i][j] * dn2_dz_array[i][j] + np.conjugate(e1_rightz[i][j]) * e2_rightz[i][j] * \
                dn2_dz_array[i][j]

c01 = c01l + c01r
c02 = c02l + c02r
c12 = c12l + c12r

c01 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta1) * c01 * (dx * dy)
c02 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta2) * c02 * (dx * dy)
c12 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta1 - beta2) * c12 * (dx * dy)

print (c01)
print (c02)
print (c12)

