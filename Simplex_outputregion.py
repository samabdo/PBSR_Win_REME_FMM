from reme import *
import reme
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

set_num_slab_modes(50)
res = 2001
reme.rememode.set_samplings(res,res)

# Define materials (at 1550nm)
wavelength = 1.55e-6
reme.set_wavelength(wavelength)

m_si = Material(3.455, 'm_si')
m_sio2 = Material(1.445, 'm_sio2')
m_sin = Material(2.0, 'm_sin')
m_air = Material(1, 'm_air')

t_substrate = 1e-6  # thickness of the SiO2 substrate
t_core = 220e-9  # thickness of the core
t_cladding = 1e-6  # thickness of the top cladding
dz = 2e-6

dz = 0.5e-6

w00 = 0.43e-6
w0 = 0.54e-6
w3 = 0.86e-6
w4 = 0.54e-6
wg1 = 0.15e-6
wg2 = 2e-6

wcn0 = w00
wcw0 = w3
wcn2 = w0
wcw2 = w4
wgp0 = wg1
wgp2 = wg2
####### Parameter definition
# w[0] = wcn2
# w[1] = wcw2
###########################

def coupling_co(w):  ###a function to calculate the coupling coefficient of a linear taper
    print '===================================================='
    ##### 2 - Define positions for wg, wg_0 and wg_2. Also the width if the strips
    wtot = 6e-6
    ycentre = wtot / 2  ###ridge centre y location
    xsin = t_substrate + t_core / 2  ###ridge centre x location of wg

    wcn1 = (wcn0 + w[0]) / 2
    wcw1 = (wcw0 + w[1]) / 2
    wgp1 = (wgp0 + w[2]) / 2

    yr1 = ycentre + 0.5 * wgp1
    yl1 = ycentre - 0.5 * wgp1
    yr0 = ycentre + 0.5 * wgp0
    yl0 = ycentre - 0.5 * wgp0
    yr2 = ycentre + 0.5 * w[2]
    yl2 = ycentre - 0.5 * w[2]

    yleft1 = yl1 - wcn1
    yright1 = yr1 + wcw1
    yleft0 = yl0 - wcn0
    yright0 = yr0 + wcw0
    yleft2 = yl2 - w[0]
    yright2 = yr2 + w[1]

    wsl = yleft0 - yleft2  ###width of left stripe
    wsr = yright0 - yright2  ###width of right stripe
    wsml = yl0 - yl2
    wsmr = yr2 - yr0
    stripe_t = t_core  ###Stripes height
    print 'left stripe width =', abs(wsl)
    print 'right stripe width =', abs(wsr)
    print 'middle left stripe width =', abs(wsml)
    print 'middle right stripe width =', abs(wsmr)
    #############################################################################

    ##### 3 - Define wg_1
    s_core = reme.Slab(m_sio2(t_substrate) + m_si(t_core) + m_sin(t_cladding))
    s_clad = reme.Slab(m_sio2(t_substrate) + m_sin(t_core + t_cladding))
    rwg_wire = reme.RWG(s_clad(yleft1) + s_core(wcn1) + s_clad(wgp1) + s_core(wcw1) + s_clad(wtot - yright1))
    guide = reme.FMMStraight(rwg_wire)
    guide.set_matching_point(4.3)
    guide.set_left_boundary(reme.PEC)
    guide.set_right_boundary(reme.PEC)
    guide.set_top_boundary(reme.PEC)
    guide.set_bottom_boundary(reme.PEC)
    guide.set_degenerate(True)
    guide.polish_mode_list()
    guide.find_modes(5, m_si.n().real, m_sio2.n().real, scan_step=1e-3)

    # #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% making sure modes are correct. from previous simulations some modes get missed or calculated incorrectly
    # neff0 = guide.get_mode_effective_index(0).real
    # neff1 = guide.get_mode_effective_index(1).real
    # match = 3.9
    # while((neff0 < 2.5 or neff1 < 2.4) and match <= 4.9):
    #     print '%%%%%%%%%%%%%%%%%%%%%%%%wtfwtfwf%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
    #     match += 0.1
    #     guide.set_matching_point(match)
    #     guide.set_left_boundary(reme.PEC)
    #     guide.set_right_boundary(reme.PEC)
    #     guide.set_top_boundary(reme.PEC)
    #     guide.set_bottom_boundary(reme.PEC)
    #     guide.set_degenerate(True)
    #     guide.polish_mode_list()
    #     guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    #     neff0 = guide.get_mode_effective_index(0).real
    #     neff1 = guide.get_mode_effective_index(1).real
    #
    #
    # match = 1.9
    # while ((neff0 < 2.5 or neff1 < 2.4) and match <= 2.9):
    #     print '$$$$$$$$$$$$$$$wtfwtfwf$$$$$$$$$$$$$$$$$$$$$$$$$$'
    #     match += 0.1
    #     guide.set_matching_point(match)
    #     guide.set_left_boundary(reme.PEC)
    #     guide.set_right_boundary(reme.PEC)
    #     guide.set_top_boundary(reme.PEC)
    #     guide.set_bottom_boundary(reme.PEC)
    #     guide.set_degenerate(True)
    #     guide.polish_mode_list()
    #     guide.find_modes(3, m_si.n().real, m_sio2.n().real, scan_step=1e-3)
    #     neff0 = guide.get_mode_effective_index(0).real
    #     neff1 = guide.get_mode_effective_index(1).real
    # #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ########################

    ####### 4 - Defining wg_0 and wg_2

    wg_0 = reme.RWG(s_clad(yleft0) + s_core(wcn0) + s_clad(wgp0) + s_core(wcw0) + s_clad(wtot - yright0))
    wg_2 = reme.RWG(s_clad(yleft2) + s_core(w[0]) + s_clad(w[2]) + s_core(w[1]) + s_clad(wtot - yright2))

    #####################################

    ####### 5 - Getting n0 and n2, and calculating dn2/dz
    nxx = 100
    nyy = 100
    n0l = 2.0  ###SiN refractive index
    n2l = 3.455  ###Si refractive index
    dn2_dz = (n2l ** 2 - n0l ** 2) / (2 * dz)
    print 'dn2/dz =', dn2_dz

    ###################################################################

    ######## 8 - Calculating betas and defining constants
    k0 = 2.0 * np.pi / wavelength
    epsilon0 = 8.854187817e-12
    mu0 = 4.0e-7 * np.pi
    beta0 = k0 * guide.get_mode_effective_index(0).real
    beta1 = k0 * guide.get_mode_effective_index(1).real
    beta2 = k0 * guide.get_mode_effective_index(2).real
    dx = stripe_t / (nxx - 1)
    dy = wsl / (nyy - 1)
    ################################################

    ####### 9 - Constructing vectors for electric fields with exactly the same width and vector sizes as stripes
    xleft = xright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    xmleft = xmright = np.linspace(t_substrate, t_substrate + t_core, nxx)
    if yleft0 > yleft2:
        yleft = np.linspace(yleft2, yleft0, nyy)
        dn2_dz_arrayl = abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        yleft = np.linspace(yleft0, yleft2, nyy)
        dn2_dz_arrayl = -1 * abs(dn2_dz) * np.ones((nxx, nyy))

    if yright2 > yright0:
        yright = np.linspace(yright0, yright2, nyy)
        dn2_dz_arrayr = abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        yright = np.linspace(yright2, yright0, nyy)
        dn2_dz_arrayr = -1 * abs(dn2_dz) * np.ones((nxx, nyy))

    if yl0 > yl2:
        ymleft = np.linspace(yl2, yl0, nyy)
        dn2_dz_arrayml = -1 * abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        ymleft = np.linspace(yl0, yl2, nyy)
        dn2_dz_arrayml = abs(dn2_dz) * np.ones((nxx, nyy))

    if yr2 > yr0:
        ymright = np.linspace(yr0, yr2, nyy)
        dn2_dz_arraymr = -1 * abs(dn2_dz) * np.ones((nxx, nyy))
    else:
        ymright = np.linspace(yr2, yr0, nyy)
        dn2_dz_arraymr = abs(dn2_dz) * np.ones((nxx, nyy))

    e0_leftx = np.zeros((nxx, nyy), complex)
    e1_leftx = np.zeros((nxx, nyy), complex)
    e2_leftx = np.zeros((nxx, nyy), complex)
    e0_lefty = np.zeros((nxx, nyy), complex)
    e1_lefty = np.zeros((nxx, nyy), complex)
    e2_lefty = np.zeros((nxx, nyy), complex)
    e0_leftz = np.zeros((nxx, nyy), complex)
    e1_leftz = np.zeros((nxx, nyy), complex)
    e2_leftz = np.zeros((nxx, nyy), complex)
    e0_rightx = np.zeros((nxx, nyy), complex)
    e1_rightx = np.zeros((nxx, nyy), complex)
    e2_rightx = np.zeros((nxx, nyy), complex)
    e0_righty = np.zeros((nxx, nyy), complex)
    e1_righty = np.zeros((nxx, nyy), complex)
    e2_righty = np.zeros((nxx, nyy), complex)
    e0_rightz = np.zeros((nxx, nyy), complex)
    e1_rightz = np.zeros((nxx, nyy), complex)
    e2_rightz = np.zeros((nxx, nyy), complex)

    e0_mleftx = np.zeros((nxx, nyy), complex)
    e1_mleftx = np.zeros((nxx, nyy), complex)
    e2_mleftx = np.zeros((nxx, nyy), complex)
    e0_mlefty = np.zeros((nxx, nyy), complex)
    e1_mlefty = np.zeros((nxx, nyy), complex)
    e2_mlefty = np.zeros((nxx, nyy), complex)
    e0_mleftz = np.zeros((nxx, nyy), complex)
    e1_mleftz = np.zeros((nxx, nyy), complex)
    e2_mleftz = np.zeros((nxx, nyy), complex)
    e0_mrightx = np.zeros((nxx, nyy), complex)
    e1_mrightx = np.zeros((nxx, nyy), complex)
    e2_mrightx = np.zeros((nxx, nyy), complex)
    e0_mrighty = np.zeros((nxx, nyy), complex)
    e1_mrighty = np.zeros((nxx, nyy), complex)
    e2_mrighty = np.zeros((nxx, nyy), complex)
    e0_mrightz = np.zeros((nxx, nyy), complex)
    e1_mrightz = np.zeros((nxx, nyy), complex)
    e2_mrightz = np.zeros((nxx, nyy), complex)

    for i in range(0, nxx):
        for j in range(0, nyy):
            ####field at left stripe
            e0_leftx[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).x
            e1_leftx[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).x
            e2_leftx[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).x
            e0_lefty[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).y
            e1_lefty[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).y
            e2_lefty[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).y
            e0_leftz[i][j] = guide.get_E_field(0, xleft[i], yleft[j]).z
            e1_leftz[i][j] = guide.get_E_field(1, xleft[i], yleft[j]).z
            e2_leftz[i][j] = guide.get_E_field(2, xleft[i], yleft[j]).z
            ####field at right stripe
            e0_rightx[i][j] = guide.get_E_field(0, xright[i], yright[j]).x
            e1_rightx[i][j] = guide.get_E_field(1, xright[i], yright[j]).x
            e2_rightx[i][j] = guide.get_E_field(2, xright[i], yright[j]).x
            e0_righty[i][j] = guide.get_E_field(0, xright[i], yright[j]).y
            e1_righty[i][j] = guide.get_E_field(1, xright[i], yright[j]).y
            e2_righty[i][j] = guide.get_E_field(2, xright[i], yright[j]).y
            e0_rightz[i][j] = guide.get_E_field(0, xright[i], yright[j]).z
            e1_rightz[i][j] = guide.get_E_field(1, xright[i], yright[j]).z
            e2_rightz[i][j] = guide.get_E_field(2, xright[i], yright[j]).z

            ####field at middle left stripe
            e0_mleftx[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).x
            e1_mleftx[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).x
            e2_mleftx[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).x
            e0_mlefty[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).y
            e1_mlefty[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).y
            e2_mlefty[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).y
            e0_mleftz[i][j] = guide.get_E_field(0, xmleft[i], ymleft[j]).z
            e1_mleftz[i][j] = guide.get_E_field(1, xmleft[i], ymleft[j]).z
            e2_mleftz[i][j] = guide.get_E_field(2, xmleft[i], ymleft[j]).z
            ####field at middle right stripe
            e0_mrightx[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).x
            e1_mrightx[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).x
            e2_mrightx[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).x
            e0_mrighty[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).y
            e1_mrighty[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).y
            e2_mrighty[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).y
            e0_mrightz[i][j] = guide.get_E_field(0, xmright[i], ymright[j]).z
            e1_mrightz[i][j] = guide.get_E_field(1, xmright[i], ymright[j]).z
            e2_mrightz[i][j] = guide.get_E_field(2, xmright[i], ymright[j]).z
    ##############################################################################################

    ####### 10 - Calculating coupling coefficient
    c01l = 0.0
    c02l = 0.0
    c12l = 0.0
    c01r = 0.0
    c02r = 0.0
    c12r = 0.0

    c01ml = 0.0
    c02ml = 0.0
    c12ml = 0.0
    c01mr = 0.0
    c02mr = 0.0
    c12mr = 0.0

    c01 = 0.0
    c02 = 0.0
    c12 = 0.0

    for i in range(nxx):  ###Dot product of the fields at the left and right stripes
        for j in range(nyy):
            c01l += np.conjugate(e0_leftx[i][j]) * e1_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j]) * \
                    e1_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j]) * e1_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c02l += np.conjugate(e0_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_lefty[i][j]) * \
                    e2_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e0_leftz[i][j]) * e2_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c12l += np.conjugate(e1_leftx[i][j]) * e2_leftx[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e1_lefty[i][j]) * \
                    e2_lefty[i][j] * dn2_dz_arrayl[i][j] + np.conjugate(e1_leftz[i][j]) * e2_leftz[i][j] * \
                    dn2_dz_arrayl[i][j]
            c01r += np.conjugate(e0_rightx[i][j]) * e1_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e0_righty[i][j]) * e1_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j]) * e1_rightz[i][
                        j] * dn2_dz_arrayr[i][j]
            c02r += np.conjugate(e0_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e0_righty[i][j]) * e2_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e0_rightz[i][j]) * e2_rightz[i][
                        j] * dn2_dz_arrayr[i][j]
            c12r += np.conjugate(e1_rightx[i][j]) * e2_rightx[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(
                e1_righty[i][j]) * e2_righty[i][j] * dn2_dz_arrayr[i][j] + np.conjugate(e1_rightz[i][j]) * e2_rightz[i][
                        j] * dn2_dz_arrayr[i][j]

            c01ml += np.conjugate(e0_mleftx[i][j]) * e1_mleftx[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(
                e0_mlefty[i][j]) * e1_mlefty[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(e0_mleftz[i][j]) * \
                     e1_mleftz[i][j] * dn2_dz_arrayml[i][j]
            c02ml += np.conjugate(e0_mleftx[i][j]) * e2_mleftx[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(
                e0_mlefty[i][j]) * e2_mlefty[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(e0_mleftz[i][j]) * \
                     e2_mleftz[i][j] * dn2_dz_arrayml[i][j]
            c12ml += np.conjugate(e1_mleftx[i][j]) * e2_mleftx[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(
                e1_mlefty[i][j]) * e2_mlefty[i][j] * dn2_dz_arrayml[i][j] + np.conjugate(e1_mleftz[i][j]) * \
                     e2_mleftz[i][j] * dn2_dz_arrayml[i][j]
            c01mr += np.conjugate(e0_mrightx[i][j]) * e1_mrightx[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(
                e0_mrighty[i][j]) * e1_mrighty[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(e0_mrightz[i][j]) * \
                     e1_mrightz[i][j] * dn2_dz_arraymr[i][j]
            c02mr += np.conjugate(e0_mrightx[i][j]) * e2_mrightx[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(
                e0_mrighty[i][j]) * e2_mrighty[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(e0_mrightz[i][j]) * \
                     e2_mrightz[i][j] * dn2_dz_arraymr[i][j]
            c12mr += np.conjugate(e1_mrightx[i][j]) * e2_mrightx[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(
                e1_mrighty[i][j]) * e2_mrighty[i][j] * dn2_dz_arraymr[i][j] + np.conjugate(e1_mrightz[i][j]) * \
                     e2_mrightz[i][j] * dn2_dz_arraymr[i][j]

    c01 = c01l + c01r + c01ml + c01mr
    c02 = c02l + c02r + c02ml + c02mr
    c12 = c12l + c12r + c12ml + c12mr

    c01 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta1) * c01 * (dx * dy)
    c01 = abs(c01) / (beta0 - beta1)
    c02 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta0 - beta2) * c02 * (dx * dy)
    c02 = abs(c02) / (beta0 - beta2)
    c12 = (epsilon0 / mu0) ** 0.5 * k0 / 4.0 / (beta1 - beta2) * c12 * (dx * dy)
    c12 = abs(c12)/ (beta1 - beta2)

    # print (c01)
    # print (c02)
    if abs(c12) < 0.0999:   ###avoiding the part where c12 = 0 when w2 = w0
        c12 = 0.2
        print '%%%%%%%%%%%%%%%to2 to2%%%%%%%%%%%%%'

    if abs(c12)>=0.09 and abs(c12)<=0.1: #####Setting the termination criterion (when c12 = 0.1) with a finite tolerance
        c12 = 0
        print '*************hell yeah*************'
    print (c12)

    return c12


# def f(x):
#     z = x[0]**2 + x[1]**2
#     return z
wna = (wcn0+wcn2)/2
wwa = (wcw0+wcw2)/2
wgpa = (wgp0+wgp2)/2
wi = [wna,wwa,wgpa]
minimum = optimize.fmin(coupling_co, wi)




# minimum[0]